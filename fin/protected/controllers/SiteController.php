<?php

class SiteController extends ControllerFree {
	public function actions() {
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array(
					'class' => 'CCaptchaAction','backColor' => 0xFFFFFF,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page' => array(
					'class' => 'CViewAction',
				),
		);
	}
	public function actionPohon() {
		if(Yii::app()->user->isGuest){
			$this->redirect(array("login"));
		}
		$this->render('pohon');
	}
	public function actionKlasifikasiGuru() {
			if(Yii::app()->user->isGuest){
			$this->redirect(array("login"));
		}
		$this->render('klasifikasiguru');
	}
	public function actionIndex() {
			if(Yii::app()->user->isGuest){
			$this->redirect(array("login"));
		}
		$this->render('index');
	}
	
	public function actionPage($id = null) {

		if ($id != null) {
			$this->page['curentMenu'] = Menu::model()->findByAttributes(array(
						'link' => $id
					));
			$menu = &$this->page['curentMenu'];
			$this->page['title'] = $menu->title;
			;
			$this->page['description'] = $menu->description;
			$this->page['keywords'] = $menu->keywords;
			$this->render('page');
		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	public function actionAuthor($id, $s) {
		$u = Usr::model()->findByAttributes(array(
					'id' => $id
				));
		if ($u != null) {
			//$this->page['curentMenu']=&$tag;
			//$tas=TagArticle::model()->with(array('seo', 'article'))->findByAttributes(array('tagid'=>$tag->id));
			$this->page['title'] = $u->displayName();
			$this->page['description'] = $u->displayName();
			$this->page['keywords'] = $u->displayName();
			$this->render('author', array(
						'u' => $u
					));//, 'tas'=>$tas
		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	public function actionTag($id) {
		$tag = Tag::model()->findByAttributes(array(
					'link' => $id
				));
		if ($tag != null) {
			//$this->page['curentMenu']=&$tag;
			//$tas=TagArticle::model()->with(array('seo', 'article'))->findByAttributes(array('tagid'=>$tag->id));
			$this->page['title'] = $tag->title;
			$this->page['description'] = $tag->description;
			$this->page['keywords'] = $tag->keywords;
			$this->render('tag', array(
						'tag' => $tag
					));//, 'tas'=>$tas
		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	// 	public function actionTagDetail($id,$s) {
	// 		if ($id!=null) {
	// 			$this->page['curentMenu']=Tag::model()->findByAttributes(array('link'=>$id));
	// 			if ($s!=null) {
	// 				$this->page['seo']=Seo::model()->findByAttributes(array('link'=>$s));
	// 				if ($this->page['seo']!=null) {
	// 					$this->page['article']=Article::model()->findByPk($this->page['seo']->target);
	// 					$this->page['created_by']=Usr::model()->findByPk($this->page['article']->created_by);
	// 					$seo=&$this->page['seo'];
	// 					$this->page['title']=$seo->title;
	// 					$this->page['description']=$seo->description;
	// 					$this->page['keywords']=$seo->keywords;
	// 					$this->render('article');
	// 				}else{
	// 					throw new CHttpException(404,'The requested page does not exist.');
	// 				}
	// 			}else{
	// 				throw new CHttpException(404,'The requested page does not exist.');
	// 			}
	// 		}else{
	// 			throw new CHttpException(404,'The requested page does not exist.');
	// 		}

	// 	}

	public function actionCategory($id) {
		$c = CategoryArticle::model()->findByAttributes(array(
					'link' => $id
				));
		if ($c != null) {
			$this->page['title'] = $c->title;
			$this->page['description'] = $c->description;
			$this->page['keywords'] = $c->keywords;
			$this->render('category', array(
						'c' => $c
					));//, 'tas'=>$tas
		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}

	// 	public function actionCategoryDetail($id,$s) {
	// 		if ($id!=null) {
	// 			$this->page['curentMenu']=CategoryArticle::model()->findByAttributes(array('link'=>$id));
	// 			if ($s!=null) {
	// 				$this->page['seo']=Seo::model()->findByAttributes(array('link'=>$s));
	// 				if ($this->page['seo']!=null) {
	// 					$this->page['article']=Article::model()->findByPk($this->page['seo']->target);
	// 					$this->page['created_by']=Usr::model()->findByPk($this->page['article']->created_by);
	// 					$seo=&$this->page['seo'];
	// 					$this->page['title']=$seo->title;
	// 					$this->page['description']=$seo->description;
	// 					$this->page['keywords']=$seo->keywords;
	// 					$this->render('article');
	// 				}else{
	// 					throw new CHttpException(404,'The requested page does not exist.');
	// 				}
	// 			}else{
	// 				throw new CHttpException(404,'The requested page does not exist.');
	// 			}
	// 		}else{
	// 			throw new CHttpException(404,'The requested page does not exist.');
	// 		}

	// 	}

	public function actionArticle($id) {
		if ($id != null) {
			$this->page['seo'] = Seo::model()->findByAttributes(array(
						'link' => $id
					));
			if ($this->page['seo'] != null) {
				$this->page['article'] = Article::model()->findByPk($this->page['seo']->target);
				$this->page['created_by'] = Usr::model()->findByPk($this->page['article']->created_by);
				$seo = &$this->page['seo'];
				$this->page['title'] = $seo->title;
				$this->page['description'] = $seo->description;
				$this->page['keywords'] = $seo->keywords;
				$this->render('article');
			} else {
				throw new CHttpException(404, 'The requested page does not exist.');
			}

		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}

	}

	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$this->title = 'Home';
		$this->buildNavs();
		$this->layout = '//layouts/main';

		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$headers = "From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
				Yii::app()->user
						->setFlash('contact',
								'<strong>Message sent!   </strong>Thank you for contacting us. We will respond to you as soon as possible.');

				$this->refresh();

			}
		}
		$this->render('contact', array(
					'model' => $model
				));
	}

	public function actionLogin() {
		$this->layout = '//layouts/login';
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		$this->ajaxEnd();
		$this->page['title'] = 'Login - ' . Yii::app()->name;
		$this->page['keywords'] = 'login, ' . Yii::app()->name . ', login ' . Yii::app()->name;
		$this->page['description'] = 'Halaman untuk login ' . Yii::app()->name;

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				Yii::app()->user->setFlash('success', '<strong>Logged In!</strong>');
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form

		if (!empty($_POST['LoginForm'])) {
			Yii::app()->user->setFlash('error', '<strong>Not Logged In.</strong>Wrong Credentials.');
		}
		$this->render('login', array(
					'model' => $model
				));

	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionProduct($id, $s) {
		$m = Category::model()->findByPk((int) $id);
		if ($m != null) {
			$ps = new CActiveDataProvider('Product',
					array(
						'pagination' => array(
							'pageSize' => 12
						)
					));
			$this->render('product', array(
						'm' => $m,'ps' => $ps
					));
		}
	}
	public function actionAll() {
		$this->render('all');
	}
	public function actionOne() {
		$this->render('one');
	}
	public function actionItem($id, $s) {
		$p = Product::model()->findByPk((int) $id);
		if ($p != null) {
			$this->render('item', array(
						'p' => $p
					));
		} else {
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	public function hitungGsa($gurus, $nilaiPrestasis, $nilaiKompetensiPadaggigs, $entropiAwal, $name, $index_col) {

		$jumlahNilaiKompetensiPadaggig = array();
		$jumlahNilaiKompetensiPadaggigNp = array();
		foreach ($nilaiPrestasis as $v) {
			$jumlahNilaiKompetensiPadaggigNp[$v->id] = array();
		}

		foreach ($nilaiKompetensiPadaggigs as $v) {
			$jumlahNilaiKompetensiPadaggig[$v->id] = 0;
			foreach ($nilaiPrestasis as $v2) {
				$jumlahNilaiKompetensiPadaggigNp[$v2->id][$v->id] = 0;
			}
		}
		$jumlahGuru = 0;
		foreach ($gurus as $v) {
			$jumlahGuru++;
			foreach ($nilaiKompetensiPadaggigs as $v2) {
				if ($index_col == 0) {
					if ($v->nilai_kompetensi_padagogig_id == $v2->id) {
						$jumlahNilaiKompetensiPadaggig[$v2->id]++;
						foreach ($nilaiPrestasis as $v3) {
							if ($v->nilai_prestasi_id == $v3->id) {
								$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
							}
						}
					}
				} else if ($index_col == 1) {
					if ($v->nilai_kompetensi_pengembangan_guru_id == $v2->id) {
						$jumlahNilaiKompetensiPadaggig[$v2->id]++;
						foreach ($nilaiPrestasis as $v3) {
							if ($v->nilai_prestasi_id == $v3->id) {
								$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
							}
						}
					}
				} else if ($index_col == 2) {
					if ($v->nilai_kepribadian_dan_sosial_guru_id == $v2->id) {
						$jumlahNilaiKompetensiPadaggig[$v2->id]++;
						foreach ($nilaiPrestasis as $v3) {
							if ($v->nilai_prestasi_id == $v3->id) {
								$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
							}
						}
					}
				} else if ($index_col == 3) {
					if ($v->nilai_masa_jabatan_id == $v2->id) {
						$jumlahNilaiKompetensiPadaggig[$v2->id]++;
						foreach ($nilaiPrestasis as $v3) {
							if ($v->nilai_prestasi_id == $v3->id) {
								$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
							}
						}
					}
				} else if ($index_col == 4) {
					if ($v->nilai_status_pandidikan_terakhir_id == $v2->id) {
						$jumlahNilaiKompetensiPadaggig[$v2->id]++;
						foreach ($nilaiPrestasis as $v3) {
							if ($v->nilai_prestasi_id == $v3->id) {
								$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
							}
						}
					}
				}

			}
		}
		
		//Yii::trace('Jumlah Guru = '.$jumlahGuru, 'trace-data.1' );


// 		foreach ($nilaiKompetensiPadaggigs as $v2) {
// 			echo 'Jumlah semua ' . $v2->kategori . ' :' . $jumlahNilaiKompetensiPadaggig[$v2->id];
// 			echo '<br />';
// 			foreach ($nilaiPrestasis as $v3) {
// 				echo 'Jumlah ' . $v2->kategori . ' yang ' . $v3->kategori . ' :'
// 						. $jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id];
// 				echo '<br />';
// 			}
// 		}

		$e = array();
		foreach ($nilaiKompetensiPadaggigs as $v) {
			$e[$v->id] = 0;
			$isNol = false;
			foreach ($nilaiPrestasis as $v3) {
				if ($jumlahNilaiKompetensiPadaggigNp[$v3->id][$v->id] == 0) {
					$isNol = true;
				}
			}
			if ($isNol) {
				$e[$v->id] = 0;
			} else {
				$ex = 0;
				foreach ($nilaiPrestasis as $v3) {

					$minPpositif = round(
							$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v->id] / $jumlahNilaiKompetensiPadaggig[$v->id],
							9);
					$log2Ppositif = round(
							log(
									$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v->id]
											/ $jumlahNilaiKompetensiPadaggig[$v->id], 2), 9);

					$hasil = $minPpositif * $log2Ppositif;
// 					if ($index_col == 1) {
// 						echo $minPpositif . ' x ' . $log2Ppositif . ' = ' . $hasil;
// 						echo '<br />';
// 					}

					$ex = $ex - round($hasil, 9);
				}
				$e[$v->id] = round($ex, 9);
			}
		}

		$gsa = $entropiAwal;
		foreach ($nilaiKompetensiPadaggigs as $v) {
			$gsa = $gsa - ($jumlahNilaiKompetensiPadaggig[$v->id] / $jumlahGuru * $e[$v->id]);
// 			echo $v->kategori . '-' . $e[$v->id];
// 			echo '<br />';
		}
		return round($gsa, 9);

	}

	public function hitungEntropi($gurus, $nilaiPrestasis, $name) {
		$jumlah = array();
		$jumlahGuru = 0;
		foreach ($nilaiPrestasis as $v) {
			$jumlah[$v->id] = 0;
		}
		$jumlahBerprestasi = 0;
		foreach ($gurus as $v) {
			$jumlahGuru++;
			foreach ($nilaiPrestasis as $v2) {
				if ($v->nilai_prestasi_id == $v2->id) {
					$jumlah[$v2->id]++;
				}
			}

		}

//		echo 'Jumlah guru :' . $jumlahGuru;
//		echo '<br />';
//		echo 'Jumlah guru all:' . count($gurus);
//		echo '<br />';

// 		foreach ($nilaiPrestasis as $v) {
// 			echo 'Jumlah ' . $v->kategori . ' :' . $jumlah[$v->id];
// 			echo '<br />';
// 		}

		foreach ($nilaiPrestasis as $v) {
			if ($jumlah[$v->id] == 0) {
// 				echo 'Jumlah Entropi  ' . $name . ' : 0';
// 				echo '<br />';
				return 0;
			}
		}
		$e = 0;
		foreach ($nilaiPrestasis as $v) {
			$jumlah[$v->id];
			$minPpositif = round($jumlah[$v->id] / $jumlahGuru, 9);
			$log2Ppositif = round(log($jumlah[$v->id] / $jumlahGuru, 2), 9);
			$hasil = round($minPpositif * $log2Ppositif, 9);
			$e = $e - $hasil;
		}

		$e = round($e, 9);
		$entropiAwal = $e;

// 		echo 'Jumlah Entropi  ' . $name . ' :' . $entropiAwal;
// 		echo '<br />';
		return $entropiAwal;
	}

	// 	public function hitungGsa($gurus, $nilaiPrestasis, $nilaiKompetensiPadaggigs,  $entropiAwal, $name){
	// 		$jumlahNilaiKompetensiPadaggig=array();
	// 		$jumlahNilaiKompetensiPadaggigNp=array();
	// 		foreach ($nilaiPrestasis as $v) {
	// 			$jumlahNilaiKompetensiPadaggigNp[$v->id]=array();
	// 		}
	// // 		$jumlahNilaiKompetensiPadaggigBerprestasi=array();
	// // 		$jumlahNilaiKompetensiPadaggigBelumBerprestasi=array();
	// 		foreach ($nilaiKompetensiPadaggigs as $v) {
	// 			$jumlahNilaiKompetensiPadaggig[$v->id]=0;
	// 			foreach ($nilaiPrestasis as $v2) {
	// 				$jumlahNilaiKompetensiPadaggigNp[$v2->id][$v->id]=0;
	// 			}
	// // 			$jumlahNilaiKompetensiPadaggigBerprestasi[$v->id]=0;
	// // 			$jumlahNilaiKompetensiPadaggigBelumBerprestasi[$v->id]=0;
	// 		}
	// // 		foreach ($gurus as $v) {
	// // 			foreach ($nilaiKompetensiPadaggigs as $v2) {
	// // 				if ($v->nilai_kompetensi_padagogig_id==$v2->id) {
	// // 					$jumlahNilaiKompetensiPadaggig[$v2->id]++;
	// // 					foreach ($nilaiPrestasis as $v3) {
	// // 						if ($v->nilai_prestasi_id==$v3->id) {
	// // 							$jumlahNilaiKompetensiPadaggigNp[$v3->id][$v2->id]++;
	// // 						}
	// // 					}
	// // 				}
	// // 			}
	// // 		}

	// 		$eNilaiKompetensiPadaggig=array();
	// 		foreach ($nilaiKompetensiPadaggigs as $v) {
	// 			$eNilaiKompetensiPadaggig[$v->id]=$this->hitungEntropi($gurus, $nilaiKompetensiPadaggigs, $name);
	// // 			$eNilaiKompetensiPadaggig[$v->id]=$this->hitungEntropi($jumlahNilaiKompetensiPadaggig[$v->id], $jumlahNilaiKompetensiPadaggigBerprestasi[$v->id],
	// // 					$jumlahNilaiKompetensiPadaggigBelumBerprestasi[$v->id], 'Nilai kompetensi padagogig '.$v->kategori);

	// 		}

	// 		$gsa=0;
	// 		$gsa=$entropiAwal;
	// 		$jumlahGuru=count($gurus);
	// 		foreach ($nilaiKompetensiPadaggigs as $v) {
	// 			$gsa=$gsa-($jumlahNilaiKompetensiPadaggig[$v->id]/$jumlahGuru*$eNilaiKompetensiPadaggig[$v->id]);
	// 		}
	// 		return  $gsa;
	// 	}

}
