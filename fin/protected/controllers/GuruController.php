<?php

class GuruController extends Controller {
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column_2';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this->render('view', array(
					'model' => $this->loadInternModel($id),
				));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new Guru;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Guru'])) {
			$model->attributes = $_POST['Guru'];
			if ($model->validate() && $model->save())
				$this->redirect(array(
							'view','id' => $model->id
						));
		}

		$this->render('create', array(
					'model' => $model,
				));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model = $this->loadInternModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Guru'])) {
			$model->attributes = $_POST['Guru'];
			if ($model->save())
				$this->redirect(array(
							'view','id' => $model->id
						));
		}

		$this->render('update', array(
					'model' => $model,
				));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadInternModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array(
							'admin'
						));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Guru');
		$this->render('index', array(
					'dataProvider' => $dataProvider,
				));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$model = new Guru('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Guru']))
			$model->attributes = $_GET['Guru'];

		$this->render('admin', array(
					'model' => $model,
				));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Guru the loaded model
	 * @throws CHttpException
	 */
	public function loadInternModel($id) {
		$model = Guru::model()->with(array('nilaiKompetensiPadagogig', 'nilaiStatusPandidikanTerakhir', 'nilaiKompetensiPengembanganGuru', 'nilaiKepribadianDanSosialGuru', 'nilaiMasaJabatan', 'nilaiPrestasi'))->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Guru $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'guru-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
