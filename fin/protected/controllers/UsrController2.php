<?php

class UsrController2 extends ControllerSecure {
	public function actionView($id) {
		$this->render('view', array(
					'model' => $this->loadModel($id),
				));
	}

	public function actionCreate() {
		$model = new Usr;

		$this->performAjaxValidation($model);

		if (isset($_POST['Usr'])) {
			$model->attributes = $_POST['Usr'];
			$password_inputan = $model->password;
			$model->password = $model->md5($model->password);

			if ($model->save()) {
				$this->redirect(array(
							'view','id' => $model->id
						));
			} else {
				$model->password = $password_inputan;
			}
		}

		$this->render('create', array(
					'model' => $model,
				));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id);

		$this->performAjaxValidation($model);

		if (isset($_POST['Usr'])) {
			$model->attributes = $_POST['Usr'];
			if ($model->save())
				$this->redirect(array(
							'view','id' => $model->id
						));
		}

		$this->render('update', array(
					'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array(
							'admin'
						));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	public function actionIndex() {
		$model = new Usr('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Usr']))
			$model->attributes = $_GET['Usr'];

		$this->render('index', array(
					'model' => $model,
				));
	}

	public function loadModel($id) {
		$model = Usr::model()->with(array(
					'type0','privilege0'
				))->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'usr-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
