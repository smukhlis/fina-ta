<?php
/* @var $this StatusPendidikanTerakhirController */
/* @var $model StatusPendidikanTerakhir */
?>

<?php
$this->breadcrumbs = array(
	'Status Pendidikan Terakhirs' => array(
		'index'
	),$model->id,
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage StatusPendidikanTerakhir','url' => array(
				'admin'
			)
		),
		array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create StatusPendidikanTerakhir','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-edit','label' => 'Update StatusPendidikanTerakhir',
				'url' => array(
					'update','id' => $model->id
				)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete StatusPendidikanTerakhir','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>

<?php echo BSHtml::pageHeader('View', 'StatusPendidikanTerakhir ' . $model->id) ?>

<?php $this
		->widget('zii.widgets.CDetailView',
				array(
						'htmlOptions' => array(
							'class' => 'table table-striped table-condensed table-hover',
						),'data' => $model,
						'attributes' => array(
							'id','status_pendidikan_terakhir','kategori',
						),
				));
?>