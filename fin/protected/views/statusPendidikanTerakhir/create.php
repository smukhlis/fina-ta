<?php
/* @var $this StatusPendidikanTerakhirController */
/* @var $model StatusPendidikanTerakhir */
?>

<?php
$this->breadcrumbs = array(
	'Status Pendidikan Terakhirs' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage StatusPendidikanTerakhir','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'StatusPendidikanTerakhir') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>