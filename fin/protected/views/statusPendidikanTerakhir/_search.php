<?php
/* @var $this StatusPendidikanTerakhirController */
/* @var $model StatusPendidikanTerakhir */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this
		->beginWidget('bootstrap.widgets.BsActiveForm',
				array(
					'action' => Yii::app()->createUrl($this->route),'method' => 'get',
				));
	?>

                    <?php echo $form->textFieldControlGroup($model, 'id'); ?>

                    <?php echo $form
		->textFieldControlGroup($model, 'status_pendidikan_terakhir', array(
			'maxlength' => 255
		)); ?>

                    <?php echo $form->textFieldControlGroup($model, 'kategori', array(
			'maxlength' => 255
		)); ?>

        <div class="form-actions">
        <?php echo BSHtml::submitButton('Search', array(
	'color' => BSHtml::BUTTON_COLOR_PRIMARY,
)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->