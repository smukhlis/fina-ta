<?php
/* @var $this StatusPendidikanTerakhirController */
/* @var $model StatusPendidikanTerakhir */
?>

<?php
$this->breadcrumbs = array(
	'Status Pendidikan Terakhirs' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage StatusPendidikanTerakhir','url' => array(
				'admin'
			)
		),
		array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create StatusPendidikanTerakhir','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete StatusPendidikanTerakhir','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'StatusPendidikanTerakhir ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>