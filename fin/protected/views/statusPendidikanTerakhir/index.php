<?php
/* @var $this StatusPendidikanTerakhirController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Status Pendidikan Terakhirs',
);

$this->menu = array(
		array(
			'label' => 'Create StatusPendidikanTerakhir','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage StatusPendidikanTerakhir','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Status Pendidikan Terakhirs') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>