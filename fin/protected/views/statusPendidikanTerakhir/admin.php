<?php
/* @var $this SPTController */
/* @var $model SPT */

$this->breadcrumbs = array(
	'Status Pendidikan Terakhirs' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage SPT','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create SPT','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Status Pendidikan Terakhir') ?>
<div class="panel panel-default">
    <div class="panel-body">
        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'status-pendidikan-terakhir-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								'id','status_pendidikan_terakhir','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
