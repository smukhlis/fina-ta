<?php
/* @var $this NilaiKepribadianDanSosialGuruController */
/* @var $model NilaiKepribadianDanSosialGuru */

$this->breadcrumbs = array(
	'Nilai Kepribadian Dan Sosial Gurus' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKS','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NKS','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Nilai Kepribadian Dan Sosial Gurus') ?>
<div class="panel panel-default">

    <div class="panel-body">
        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'nilai-kepribadian-dan-sosial-guru-grid','dataProvider' => $model->search(),
						'filter' => $model,
						'columns' => array(
								'id','nilai_padagogig','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
