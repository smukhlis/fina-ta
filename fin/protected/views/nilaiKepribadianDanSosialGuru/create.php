<?php
/* @var $this NilaiKepribadianDanSosialGuruController */
/* @var $model NilaiKepribadianDanSosialGuru */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kepribadian Dan Sosial Gurus' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiKepribadianDanSosialGuru','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'NilaiKepribadianDanSosialGuru') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>