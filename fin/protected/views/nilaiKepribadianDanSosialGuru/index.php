<?php
/* @var $this NilaiKepribadianDanSosialGuruController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kepribadian Dan Sosial Gurus',
);

$this->menu = array(
		array(
			'label' => 'Create NilaiKepribadianDanSosialGuru','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage NilaiKepribadianDanSosialGuru','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Nilai Kepribadian Dan Sosial Gurus') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>