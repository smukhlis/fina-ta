<?php
/* @var $this NilaiKepribadianDanSosialGuruController */
/* @var $model NilaiKepribadianDanSosialGuru */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kepribadian Dan Sosial Gurus' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiKepribadianDanSosialGuru','url' => array(
				'admin'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NilaiKepribadianDanSosialGuru',
				'url' => array(
					'create'
				)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NilaiKepribadianDanSosialGuru',
				'url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'NilaiKepribadianDanSosialGuru ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>