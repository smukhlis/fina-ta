<?php
/* @var $this PrestasiController */
/* @var $model Prestasi */
?>

<?php
$this->breadcrumbs = array(
	'Prestasis' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage Prestasi','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create Prestasi','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete Prestasi','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'Prestasi ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>