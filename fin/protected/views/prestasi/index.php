<?php
/* @var $this PrestasiController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Prestasis',
);

$this->menu = array(
	array(
		'label' => 'Create Prestasi','url' => array(
			'create'
		)
	),array(
		'label' => 'Manage Prestasi','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Prestasis') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>