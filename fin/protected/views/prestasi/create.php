<?php
/* @var $this PrestasiController */
/* @var $model Prestasi */
?>

<?php
$this->breadcrumbs = array(
	'Prestasis' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage Prestasi','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'Prestasi') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>