<?php
/* @var $this NKPGController */
/* @var $model NKPG */

$this->breadcrumbs = array(
	'Nilai Kompetensi Pengembangan Guru' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKPG','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NKPG','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Nilai Kompetensi Pengembangan Guru') ?>
<div class="panel panel-default">
    <div class="panel-body">

        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'nilai-kompetensi-pengembangan-guru-grid','dataProvider' => $model->search(),
						'filter' => $model,
						'columns' => array(
								'id','nilai_padagogig','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
