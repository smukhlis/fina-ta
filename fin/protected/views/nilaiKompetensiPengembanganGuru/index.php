<?php
/* @var $this NilaiKompetensiPengembanganGuruController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Pengembangan Gurus',
);

$this->menu = array(
		array(
			'label' => 'Create NilaiKompetensiPengembanganGuru','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage NilaiKompetensiPengembanganGuru','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Nilai Kompetensi Pengembangan Gurus') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>