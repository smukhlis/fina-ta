<?php
/* @var $this NilaiKompetensiPengembanganGuruController */
/* @var $model NilaiKompetensiPengembanganGuru */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Pengembangan Guru' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKPG','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'Nilai Kompetensi Pengembangan Guru') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>