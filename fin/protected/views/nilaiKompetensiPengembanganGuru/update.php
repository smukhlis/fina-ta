<?php
/* @var $this NKPGController */
/* @var $model NKPG */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Pengembangan Guru' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKPG','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NKPG','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NKPG','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'Nilai Kompetensi Pengembangan Guru ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>