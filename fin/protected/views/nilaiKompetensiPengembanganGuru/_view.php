<?php
/* @var $this NilaiKompetensiPengembanganGuruController */
/* @var $data NilaiKompetensiPengembanganGuru */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array(
	'view','id' => $data->id
)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai_padagogig')); ?>:</b>
	<?php echo CHtml::encode($data->nilai_padagogig); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategori')); ?>:</b>
	<?php echo CHtml::encode($data->kategori); ?>
	<br />

</div>