<?php
$this->page['breadcrumbs'] = array(
	'Usr Privileges' => array(
		'index'
	),$model->name => array(
		'view','id' => $model->id
	),'Update',
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrPrivilege','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrPrivilege','url' => array(
				'create'
			)
		),array(
			'label' => 'View UsrPrivilege','url' => array(
				'view','id' => $model->id
			)
		),
);
?>
<div class="container">
<h1>Update UsrPrivilege <?php echo $model->id; ?></h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>