<?php
$this->page['breadcrumbs'] = array(
	'Usr Privileges' => array(
		'index'
	),$model->name,
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrPrivilege','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrPrivilege','url' => array(
				'create'
			)
		),array(
			'label' => 'Update UsrPrivilege','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'label' => 'Delete UsrPrivilege','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<div class="container">
<h1>View UsrPrivilege #<?php echo $model->id; ?></h1>
<?php $this
		->widget('bootstrap.widgets.TbDetailView',
				array(
					'data' => $model,'attributes' => array(
						'id','name','note',
					),
				));
?>
</div>