<?php
$this->page['breadcrumbs'] = array(
	'Usr Privileges' => array(
		'index'
	),'Create',
);

$this->page['actions'] = array(
	array(
		'label' => 'List UsrPrivilege','url' => array(
			'index'
		)
	),
);
?>
<div class="container">
<h1>Create UsrPrivilege</h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>