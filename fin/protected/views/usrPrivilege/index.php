<?php
$this->page['breadcrumbs'] = array(
	'Usr Privileges'
);

$this->page['actions'] = array(
	array(
		'label' => 'Create UsrPrivilege','url' => array(
			'create'
		)
	),
);
?>

<h1>Data Usr Privileges</h1>
<?php $this
		->widget('bootstrap.widgets.TbGridView',
				array(
						'id' => 'usr-privilege-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
							'id','name','note',array(
								'class' => 'bootstrap.widgets.TbButtonColumn',
							),
						),
				));
?>
