<?php
$this->page['breadcrumbs'] = array(
	'Usr Types'
);

$this->page['actions'] = array(
	array(
		'label' => 'Create UsrType','url' => array(
			'create'
		)
	),
);
?>

<h1>Data Usr Types</h1>
<?php $this
		->widget('bootstrap.widgets.TbGridView',
				array(
						'id' => 'usr-type-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
							'id','name','note',array(
								'class' => 'bootstrap.widgets.TbButtonColumn',
							),
						),
				));
?>
