<?php
$this->page['breadcrumbs'] = array(
	'Usr Types' => array(
		'index'
	),$model->name,
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrType','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrType','url' => array(
				'create'
			)
		),array(
			'label' => 'Update UsrType','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'label' => 'Delete UsrType','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<div class="container">
<h1>View UsrType #<?php echo $model->id; ?></h1>
<?php $this
		->widget('bootstrap.widgets.TbDetailView',
				array(
					'data' => $model,'attributes' => array(
						'id','name','note',
					),
				));
?>
</div>