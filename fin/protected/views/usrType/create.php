<?php
$this->page['breadcrumbs'] = array(
	'Usr Types' => array(
		'index'
	),'Create',
);

$this->page['actions'] = array(
	array(
		'label' => 'List UsrType','url' => array(
			'index'
		)
	),
);
?>
<div class="container">
<h1>Create UsrType</h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>