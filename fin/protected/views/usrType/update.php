<?php
$this->page['breadcrumbs'] = array(
	'Usr Types' => array(
		'index'
	),$model->name => array(
		'view','id' => $model->id
	),'Update',
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrType','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrType','url' => array(
				'create'
			)
		),array(
			'label' => 'View UsrType','url' => array(
				'view','id' => $model->id
			)
		),
);
?>
<div class="container">
<h1>Update UsrType <?php echo $model->id; ?></h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>