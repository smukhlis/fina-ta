<?php
/* @var $this UsrController */
/* @var $model Usr */
?>

<?php
$this->breadcrumbs=array(
	'Usrs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('icon' => 'glyphicon glyphicon-home','label'=>'Manage Usr', 'url'=>array('admin')),
array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Usr', 'url'=>array('create')),
array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Usr', 'url'=>array('update', 'id'=>$model->id)),
array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Usr', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>
<?php echo BSHtml::pageHeader('View','Usr '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
'htmlOptions' => array(
'class' => 'table table-striped table-condensed table-hover',
),
'data'=>$model,
'attributes'=>array(
		'id',
		'username',
		'password',
		'email',
		'privilege',
		'target',
		'type',
		'status',
),
)); ?>