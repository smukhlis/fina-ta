<?php
/* @var $this UsrController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Usrs',
);

$this->menu=array(
array('label'=>'Create Usr','url'=>array('create')),
array('label'=>'Manage Usr','url'=>array('admin')),
);
?>
<?php echo BSHtml::pageHeader('Usrs') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>