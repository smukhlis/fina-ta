<?php
/* @var $this UsrController */
/* @var $model Usr */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <?php echo $form->textFieldControlGroup($model,'id'); ?>

                    <?php echo $form->textFieldControlGroup($model,'username',array('maxlength'=>100)); ?>

                            <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>100)); ?>

                    <?php echo $form->textFieldControlGroup($model,'privilege'); ?>

                    <?php echo $form->textFieldControlGroup($model,'target'); ?>

                    <?php echo $form->textFieldControlGroup($model,'type'); ?>

                    <?php echo $form->textFieldControlGroup($model,'status'); ?>

        <div class="form-actions">
        <?php echo BSHtml::submitButton('Search',  array('color' => BSHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->