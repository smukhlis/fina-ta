<?php
    /* @var $this UsrController */
    /* @var $model Usr */
?>

<?php
$this->breadcrumbs=array(
	'Usrs'=>array('index'),
	'Create',
);

    $this->menu=array(
        array('icon' => 'glyphicon glyphicon-home','label'=>'Manage Usr', 'url'=>array('admin')),
    );
    ?>
<?php echo BSHtml::pageHeader('Create','Usr') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>