<?php
$this->page['breadcrumbs'] = array(
	'Usr Statuses' => array(
		'index'
	),'Create',
);

$this->page['actions'] = array(
	array(
		'label' => 'List UsrStatus','url' => array(
			'index'
		)
	),
);
?>
<div class="container">
<h1>Create UsrStatus</h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>