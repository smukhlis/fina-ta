<?php
$this->page['breadcrumbs'] = array(
	'Usr Statuses'
);

$this->page['actions'] = array(
	array(
		'label' => 'Create UsrStatus','url' => array(
			'create'
		)
	),
);
?>

<h1>Data Usr Statuses</h1>
<?php $this
		->widget('bootstrap.widgets.TbGridView',
				array(
						'id' => 'usr-status-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
							'id','name','note',array(
								'class' => 'bootstrap.widgets.TbButtonColumn',
							),
						),
				));
?>
