<?php $form = $this
		->beginWidget('bootstrap.widgets.TbActiveForm',
				array(
					'id' => 'usr-status-form','enableAjaxValidation' => true,
				));
?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'name', array(
			'class' => 'span5','maxlength' => 50
		)); ?>

	<?php echo $form->textFieldRow($model, 'note', array(
			'class' => 'span5','maxlength' => 255
		)); ?>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
<?php $this->endWidget(); ?>
