<?php
$this->page['breadcrumbs'] = array(
	'Usr Statuses' => array(
		'index'
	),$model->name => array(
		'view','id' => $model->id
	),'Update',
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrStatus','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrStatus','url' => array(
				'create'
			)
		),array(
			'label' => 'View UsrStatus','url' => array(
				'view','id' => $model->id
			)
		),
);
?>
<div class="container">
<h1>Update UsrStatus <?php echo $model->id; ?></h1>
<?php echo $this->renderPartial('_form', array(
			'model' => $model
		)); ?></div>