<?php
$this->page['breadcrumbs'] = array(
	'Usr Statuses' => array(
		'index'
	),$model->name,
);

$this->page['actions'] = array(
		array(
			'label' => 'List UsrStatus','url' => array(
				'index'
			)
		),array(
			'label' => 'Create UsrStatus','url' => array(
				'create'
			)
		),array(
			'label' => 'Update UsrStatus','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'label' => 'Delete UsrStatus','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<div class="container">
<h1>View UsrStatus #<?php echo $model->id; ?></h1>
<?php $this
		->widget('bootstrap.widgets.TbDetailView',
				array(
					'data' => $model,'attributes' => array(
						'id','name','note',
					),
				));
?>
</div>