<?php
/* @var $this NilaiPrestasiController */
/* @var $model NilaiPrestasi */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Prestasis' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiPrestasi','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'NilaiPrestasi') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>