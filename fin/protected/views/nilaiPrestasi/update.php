<?php
/* @var $this NilaiPrestasiController */
/* @var $model NilaiPrestasi */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Prestasis' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiPrestasi','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NilaiPrestasi','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NilaiPrestasi','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'NilaiPrestasi ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>