<?php
/* @var $this NilaiPrestasiController */
/* @var $model NilaiPrestasi */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Prestasi' => array(
		'index'
	),$model->id,
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage Nilai Prestasi','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create Nilai Prestasi','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-edit','label' => 'Update Nilai Prestasi',
				'url' => array(
					'update','id' => $model->id
				)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete Nilai Prestasi','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>

<?php echo BSHtml::pageHeader('View', 'Nilai Prestasi ' . $model->id) ?>

<?php $this
		->widget('zii.widgets.CDetailView',
				array(
						'htmlOptions' => array(
							'class' => 'table table-striped table-condensed table-hover',
						),'data' => $model,'attributes' => array(
							'id','nilai_prestasi','kategori',
						),
				));
?>