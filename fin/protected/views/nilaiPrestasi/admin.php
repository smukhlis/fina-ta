<?php
/* @var $this NilaiPrestasiController */
/* @var $model NilaiPrestasi */

$this->breadcrumbs = array(
	'Nilai Prestasi' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage Nilai Prestasi','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create Nilai Prestasi','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Nilai Prestasi') ?>
<div class="panel panel-default">
    <div class="panel-body">
        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'nilai-prestasi-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								'id','nilai_prestasi','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
