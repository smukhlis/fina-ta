<?php
/* @var $this NilaiPrestasiController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Prestasis',
);

$this->menu = array(
		array(
			'label' => 'Create NilaiPrestasi','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage NilaiPrestasi','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Nilai Prestasis') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>