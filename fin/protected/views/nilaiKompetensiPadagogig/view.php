<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $model NilaiKompetensiPadagogig */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Padagogig' => array(
		'index'
	),$model->id,
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKP','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NKP','url' => array(
				'create'
			)
		),array(
			'icon' => 'glyphicon glyphicon-edit','label' => 'Update NKP','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NKP','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>

<?php echo BSHtml::pageHeader('View', 'NKP ' . $model->id) ?>

<?php $this
		->widget('zii.widgets.CDetailView',
				array(
						'htmlOptions' => array(
							'class' => 'table table-striped table-condensed table-hover',
						),'data' => $model,'attributes' => array(
							'id','nilai_padagogig','kategori',
						),
				));
?>