<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $model NilaiKompetensiPadagogig */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Padagogig' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKP','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'Nilai Kompetensi Padagogig') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>