<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Padagogigs',
);

$this->menu = array(
		array(
			'label' => 'Create NilaiKompetensiPadagogig','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage NilaiKompetensiPadagogig','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Nilai Kompetensi Padagogigs') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>