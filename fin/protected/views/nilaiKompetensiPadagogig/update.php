<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $model NilaiKompetensiPadagogig */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Kompetensi Padagogigs' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiKompetensiPadagogig','url' => array(
				'admin'
			)
		),
		array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NilaiKompetensiPadagogig','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NilaiKompetensiPadagogig','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'NilaiKompetensiPadagogig ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>