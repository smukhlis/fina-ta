<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $model NilaiKompetensiPadagogig */
/* @var $form BSActiveForm */
?>

<div class="form">

    <?php $form = $this
		->beginWidget('bootstrap.widgets.BsActiveForm',
				array(
						'id' => 'nilai-kompetensi-padagogig-form',
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// There is a call to performAjaxValidation() commented in generated controller code.
						// See class documentation of CActiveForm for details on this.
						'enableAjaxValidation' => false,'layout' => BSHtml::FORM_LAYOUT_HORIZONTAL,
				));
	?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model, 'nilai_padagogig', array(
			'maxlength' => 255
		)); ?>

            <?php echo $form->textFieldControlGroup($model, 'kategori', array(
			'maxlength' => 255
		)); ?>

            <?php echo BSHtml::formActions(
		array(
			BSHtml::submitButton('Submit', array(
				'color' => BSHtml::BUTTON_COLOR_PRIMARY
			)),
		));
			?>

    <?php $this->endWidget(); ?>

</div><!-- form -->