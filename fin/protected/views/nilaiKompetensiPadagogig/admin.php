<?php
/* @var $this NilaiKompetensiPadagogigController */
/* @var $model NilaiKompetensiPadagogig */

$this->breadcrumbs = array(
	'Nilai Kompetensi Padagogigs' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NKP','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NKP','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Nilai Kompetensi Pedagogik') ?>
<div class="panel panel-default">

    <div class="panel-body">
        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'nilai-kompetensi-padagogig-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								array(
									'name' => 'id','htmlOptions' => array(
										'width' => '40px'
									),
								),'nilai_padagogig','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
