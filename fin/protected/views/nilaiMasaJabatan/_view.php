<?php
/* @var $this NilaiMasaJabatanController */
/* @var $data NilaiMasaJabatan */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array(
	'view','id' => $data->id
)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('masa_jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->masa_jabatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategori')); ?>:</b>
	<?php echo CHtml::encode($data->kategori); ?>
	<br />

</div>