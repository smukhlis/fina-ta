<?php
/* @var $this NMJController */
/* @var $model NMJ */

$this->breadcrumbs = array(
	'Nilai Masa Jabatans' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NMJ','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NMJ','url' => array(
				'create'
			)
		),
);

?>
<?php echo BSHtml::pageHeader('Manage', 'Nilai Masa Jabatan') ?>
<div class="panel panel-default">
    
    <div class="panel-body">
        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'nilai-masa-jabatan-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								'id','masa_jabatan','kategori',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
