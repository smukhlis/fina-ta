<?php
/* @var $this NMJController */
/* @var $model NMJ */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Masa Jabatan' => array(
		'index'
	),$model->id,
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NMJ','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NMJ','url' => array(
				'create'
			)
		),array(
			'icon' => 'glyphicon glyphicon-edit','label' => 'Update NMJ','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NMJ','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>

<?php echo BSHtml::pageHeader('View', 'Nilai Masa Jabatan ' . $model->id) ?>

<?php $this
		->widget('zii.widgets.CDetailView',
				array(
						'htmlOptions' => array(
							'class' => 'table table-striped table-condensed table-hover',
						),'data' => $model,'attributes' => array(
							'id','masa_jabatan','kategori',
						),
				));
?>