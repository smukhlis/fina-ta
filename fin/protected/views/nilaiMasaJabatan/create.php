<?php
/* @var $this NilaiMasaJabatanController */
/* @var $model NilaiMasaJabatan */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Masa Jabatan' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage NMJ','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'Nilai Masa Jabatan') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>