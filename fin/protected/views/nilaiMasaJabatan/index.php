<?php
/* @var $this NilaiMasaJabatanController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Masa Jabatans',
);

$this->menu = array(
		array(
			'label' => 'Create NilaiMasaJabatan','url' => array(
				'create'
			)
		),array(
			'label' => 'Manage NilaiMasaJabatan','url' => array(
				'admin'
			)
		),
);
?>
<?php echo BSHtml::pageHeader('Nilai Masa Jabatans') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>