<?php
/* @var $this NilaiMasaJabatanController */
/* @var $model NilaiMasaJabatan */
?>

<?php
$this->breadcrumbs = array(
	'Nilai Masa Jabatans' => array(
		'index'
	),$model->id => array(
		'view','id' => $model->id
	),'Update',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage NilaiMasaJabatan','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create NilaiMasaJabatan','url' => array(
				'create'
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete NilaiMasaJabatan','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>
<?php echo BSHtml::pageHeader('Update', 'NilaiMasaJabatan ' . $model->id) ?>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>