<?php $form = $this
		->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'usr-form','enableAjaxValidation' => true,
		));
?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model, 'username', array(
			'class' => 'span5','maxlength' => 100
		)); ?>

	<?php echo $form->passwordFieldRow($model, 'password', array(
			'class' => 'span5','maxlength' => 255
		)); ?>

	<?php echo $form->textFieldRow($model, 'email', array(
			'class' => 'span5','maxlength' => 100
		)); ?>

	<?php // echo $form->textFieldRow($model,'privilege',array('class'=>'span5')); ?>
	<?php
echo $form
		->select2Row($model, 'privilege',
				array(
						'asDropDownList' => true,'class' => 'span5',
						//'multiple' => 'multiple',
						'data' => CHtml::listData(UsrPrivilege::model()->findAll(), 'id', 'name'),'empty' => '',
						'options' => array(
							'placeholder' => 'Pilih Hak Akses','allowClear' => true,
						)
				));

	?>

	<?php // echo $form->textFieldRow($model,'target',array('class'=>'span5')); ?>

	<?php // echo $form->textFieldRow($model,'type',array('class'=>'span5')); ?>
	<?php
echo $form
		->select2Row($model, 'type',
				array(
						'asDropDownList' => true,'class' => 'span5',
						//'multiple' => 'multiple',
						'data' => CHtml::listData(UsrType::model()->findAll(), 'id', 'name'),'empty' => '',
						'options' => array(
							'placeholder' => 'Pilih Type','allowClear' => true,
						)
				));

	?>	

	<?php // echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>
	<?php
echo $form
		->select2Row($model, 'status',
				array(
						'asDropDownList' => true,'class' => 'span5',
						//'multiple' => 'multiple',
						'data' => CHtml::listData(UsrStatus::model()->findAll(), 'id', 'name'),'empty' => '',
						'options' => array(
							'placeholder' => 'Pilih Status','allowClear' => true,
						)
				));

	?>	

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save</button>
	</div>
<?php $this->endWidget(); ?>
