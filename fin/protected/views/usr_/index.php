<?php
$this->page['breadcrumbs'] = array(
	'Account'
);

$this->page['actions'] = array(
	array(
		'label' => 'Create Account','url' => array(
			'create'
		)
	),
);
?>

<h1>Data Account</h1>
<?php $this
		->widget('bootstrap.widgets.TbGridView',
				array(
						'id' => 'usr-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								array(
									'name' => 'id','htmlOptions' => array(
										'width' => '40px'
									),
								),'username',
								//'password',
								'email',
								array(
										'name' => 'privilege','value' => '$data->privilege0->name',
										'filter' => CHtml::listData(UsrPrivilege::model()->findAll(), 'id', 'name'),
								),
								array(
										'name' => 'type','value' => '$data->type0->name',
										'filter' => CHtml::listData(UsrType::model()->findAll(), 'id', 'name'),
								),
								//'privilege',
								//	'type',
								//'target',
								/*
								
								'status',
								 */
								array(
									'class' => 'bootstrap.widgets.TbButtonColumn',
								),
						),
				));
?>
