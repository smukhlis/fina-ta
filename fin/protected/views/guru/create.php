<?php
/* @var $this GuruController */
/* @var $model Guru */
?>

<?php
$this->breadcrumbs = array(
	'Gurus' => array(
		'index'
	),'Create',
);

$this->menu = array(
	array(
		'icon' => 'glyphicon glyphicon-home','label' => 'Manage Guru','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Create', 'Guru') ?>

<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>