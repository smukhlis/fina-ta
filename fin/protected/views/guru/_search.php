<?php
/* @var $this GuruController */
/* @var $model Guru */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this
		->beginWidget('bootstrap.widgets.BsActiveForm',
				array(
					'action' => Yii::app()->createUrl($this->route),'method' => 'get',
				));
	?>

                    <?php echo $form->textFieldControlGroup($model, 'id'); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nama', array(
			'maxlength' => 255
		)); ?>

                    <?php echo $form->textFieldControlGroup($model, 'alamat', array(
			'maxlength' => 255
		)); ?>

                    <?php echo $form->textFieldControlGroup($model, 'no_telp', array(
			'maxlength' => 255
		)); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nilai_kompetensi_padagogig_id'); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nilai_kompetensi_pengembangan_guru_id'); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nilai_kepribadian_dan_sosial_guru_id'); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nilai_masa_jabatan_id'); ?>

                    <?php echo $form->textFieldControlGroup($model, 'nilai_status_pandidikan_terakhir_id'); ?>

        <div class="form-actions">
        <?php echo BSHtml::submitButton('Search', array(
	'color' => BSHtml::BUTTON_COLOR_PRIMARY,
)); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->