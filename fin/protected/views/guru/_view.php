<?php
/* @var $this GuruController */
/* @var $data Guru */
?>

<div class="view">

    	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array(
	'view','id' => $data->id
)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama')); ?>:</b>
	<?php echo CHtml::encode($data->nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_telp')); ?>:</b>
	<?php echo CHtml::encode($data->no_telp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai_kompetensi_padagogig_id')); ?>:</b>
	<?php echo CHtml::encode($data->nilai_kompetensi_padagogig_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai_kompetensi_pengembangan_guru_id')); ?>:</b>
	<?php echo CHtml::encode($data->nilai_kompetensi_pengembangan_guru_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai_kepribadian_dan_sosial_guru_id')); ?>:</b>
	<?php echo CHtml::encode($data->nilai_kepribadian_dan_sosial_guru_id); ?>
	<br />

	<?php /*
		  <b><?php echo CHtml::encode($data->getAttributeLabel('nilai_masa_jabatan_id')); ?>:</b>
		  <?php echo CHtml::encode($data->nilai_masa_jabatan_id); ?>
		  <br />
		  
		  <b><?php echo CHtml::encode($data->getAttributeLabel('nilai_status_pandidikan_terakhir_id')); ?>:</b>
		  <?php echo CHtml::encode($data->nilai_status_pandidikan_terakhir_id); ?>
		  <br />
		  
		   */ 
	?>

</div>