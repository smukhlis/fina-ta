<?php
/* @var $this GuruController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs = array(
	'Gurus',
);

$this->menu = array(
	array(
		'label' => 'Create Guru','url' => array(
			'create'
		)
	),array(
		'label' => 'Manage Guru','url' => array(
			'admin'
		)
	),
);
?>
<?php echo BSHtml::pageHeader('Gurus') ?>
<?php $this->widget('bootstrap.widgets.BsListView', array(
			'dataProvider' => $dataProvider,'itemView' => '_view',
		));
?>