<?php
/* @var $this GuruController */
/* @var $model Guru */
?>

<?php
$this->breadcrumbs = array(
	'Gurus' => array(
		'index'
	),$model->id,
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage Guru','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create Guru','url' => array(
				'create'
			)
		),array(
			'icon' => 'glyphicon glyphicon-edit','label' => 'Update Guru','url' => array(
				'update','id' => $model->id
			)
		),
		array(
				'icon' => 'glyphicon glyphicon-minus-sign','label' => 'Delete Guru','url' => '#',
				'linkOptions' => array(
					'submit' => array(
						'delete','id' => $model->id
					),'confirm' => 'Are you sure you want to delete this item?'
				)
		),
);
?>

<?php echo BSHtml::pageHeader('View', 'Guru ' . $model->id) ?>

<?php $this
		->widget('zii.widgets.CDetailView',
				array(
						'htmlOptions' => array(
							'class' => 'table table-striped table-condensed table-hover',
						),'data' => $model,
						'attributes' => array(
								'id','nama','alamat','no_telp',
								'nilaiKompetensiPadagogig.nilai_padagogig',
								'nilaiKompetensiPengembanganGuru.nilai_padagogig',
								'nilaiKepribadianDanSosialGuru.nilai_padagogig',
								'nilaiMasaJabatan.masa_jabatan',
								'nilaiStatusPandidikanTerakhir.status_pendidikan_terakhir',
								'nilaiPrestasi.nilai_prestasi',
						),
				));
?>