<?php
/* @var $this GuruController */
/* @var $model Guru */

$this->breadcrumbs = array(
	'Gurus' => array(
		'index'
	),'Manage',
);

$this->menu = array(
		array(
			'icon' => 'glyphicon glyphicon-home','label' => 'Manage Guru','url' => array(
				'admin'
			)
		),array(
			'icon' => 'glyphicon glyphicon-plus-sign','label' => 'Create Guru','url' => array(
				'create'
			)
		),
);

Yii::app()->clientScript
		->registerScript('search',
				"
    $('.search-button').click(function(){
        $('.search-form').toggle();
            return false;
        });
        $('.search-form form').submit(function(){
            $('#guru-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });");
?>
<?php echo BSHtml::pageHeader('Manage', 'Gurus') ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo BSHtml::button('Advanced search',
		array(
			'class' => 'search-button','icon' => BSHtml::GLYPHICON_SEARCH,'color' => BSHtml::BUTTON_COLOR_PRIMARY
		), '#'); ?></h3>
    </div>
    <div class="panel-body">
        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search', array(
			'model' => $model,
		));
			?>
        </div><!-- search-form -->

        <?php $this
		->widget('bootstrap.widgets.BsGridView',
				array(
						'id' => 'guru-grid','dataProvider' => $model->search(),'filter' => $model,
						'columns' => array(
								//		'id',
								'nama',
								'alamat',
								'no_telp',
// 								'nilai_kompetensi_padagogig_id','nilai_kompetensi_pengembangan_guru_id',
// 								'nilai_kepribadian_dan_sosial_guru_id','nilai_masa_jabatan_id',
// 								'nilai_status_pandidikan_terakhir_id',
								array(
									'class' => 'bootstrap.widgets.BsButtonColumn',
								),
						),
				));
		?>
    </div>
</div>
