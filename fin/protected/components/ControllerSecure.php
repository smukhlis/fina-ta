<?php
class ControllerSecure extends CController {
	public $layout = '//layouts/main';
	public $page = array();
	public $breadcrumbs = array();
	public $menu = array();

	/* 
	 * @see CController::init()
	 */
	public function init() {
		// 	 	$this->page['p']=Yii::app()->user->getState('privilege');
		// 	 	$this->page['usrid']=Yii::app()->user->getState('id');
		// 	 	if ($this->page['p']==null || $this->page['usrid']==null) {
		// 	 		$this->redirect('site/login');
		// 	 	}
		$this->setCss();
		$this->page['title'] = 'Manage';
	}

	public function setCss() {
		$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
		$_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl . "/uploads/"; // URL for the uploads folder
		$_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath . "/../uploads/"; // path to the uploads folder
	}

	public function minLevelReader() {
		if ($this->page['p'] > 3) {
			throw new CHttpException(404, 'You do not have sufficient permissions to access this page.');
			return false;
		} else {
			return true;
		}
	}

	public function isWriter() {
		if ($this->page['p'] == 2) {
			return true;
		}
		return false;
	}

	public function isAdministrator() {
		if ($this->page['p'] == 1) {
			return true;
		}
		return false;
	}
}
