<?php
class Controller extends ControllerSecure {
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);
	}
	public function ajaxEnd() {
		if (Yii::app()->request->isAjaxRequest) {
			Yii::app()->end();
		}
	}
}
