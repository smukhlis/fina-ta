<?php
class UserIdentity extends CUserIdentity {
	public function authenticate() {
		$u1 = Usr::model()->findByAttributes(array(
					'username' => $this->username
				));
		if ($u1 == null) {
			$u1 = Usr::model()->findByAttributes(array(
						'email' => $this->username
					));
		}

		if ($u1 == null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if ($u1->password !== $u1->md5($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->errorCode = self::ERROR_NONE;
			//$this->setId($u1->id);
			$this->setState('id', $u1->id);
			$this->setState('status', $u1->status);
			$this->setState('username', $u1->username);
			$this->setState('privilege', $u1->privilege);

		}
		return !$this->errorCode;
	}

}
