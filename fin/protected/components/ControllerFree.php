<?php
class ControllerFree extends Controller {
	public $layout = '//layouts/main';
	public $page = array();
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);

	}

	/* 
	 * @see CController::init()
	 */
	public function init() {
	}

	public $thumbnailer;
	public function getThumbnailer() {
		if ($this->thumbnailer == null) {
			$this->thumbnailer = new Thumbnailer();
		}
		return $this->thumbnailer;
	}

	public function setCss() {
		// 		$cs=Yii::app()->getClientScript();
		// 		$cs->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap/css/bootstrap.css');
		// 		$cs->registerMetaTag('width=device-width, initial-scale=1.0', 'viewport');
		// 		$cs->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap/css/bootstrap-responsive.css');
		// 		$cs->registerCssFile(Yii::app()->baseUrl.'/css/bootstrap/css/bootstrap-yii.css');

		// 		$cs->registerCssFile(Yii::app()->baseUrl.'/css/style.css');

		// 		$cs->registerCoreScript('jquery');
		// 		$cs->registerScriptFile(Yii::app()->baseUrl . '/css/bootstrap/js/bootstrap.bootbox.min.js', CClientScript::POS_END);
		// 		$cs->registerScriptFile(Yii::app()->baseUrl . '/css/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
	}
}
