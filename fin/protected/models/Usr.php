<?php

/**
 * This is the model class for table "usr".
 *
 * The followings are the available columns in table 'usr':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $privilege
 * @property integer $target
 * @property integer $type
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property UsrStatus $status0
 * @property UsrPrivilege $privilege0
 * @property UsrType $type0
 */
class Usr extends CActiveRecord {

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usr the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'usr';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'password,username,email,privilege,type,status','required'
				),array(
					'privilege, target, type, status','numerical','integerOnly' => true
				),array(
					'username, email','length','max' => 100
				),array(
					'password','length','max' => 255
				),array(
					'email','email'
				),array(
					'email,username','unique','on' => 'unique'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, username, password, email, privilege, target, type, status','safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'status0' => array(
					self::BELONGS_TO,'UsrStatus','status'
				),'privilege0' => array(
					self::BELONGS_TO,'UsrPrivilege','privilege'
				),'type0' => array(
					self::BELONGS_TO,'UsrType','type'
				),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
				'id' => 'ID','username' => 'Username','password' => 'Password','email' => 'Email',
				'privilege' => 'Privilege','target' => 'Target','type' => 'Type','status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('privilege', $this->privilege);
		$criteria->compare('target', $this->target);
		$criteria->compare('type', $this->type);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this->with(array(
					'type0','privilege0'
				)), array(
			'criteria' => $criteria,
		));
	}
	public function md5($pwd) {
		return md5($pwd);
	}

	public function displayName() {
		return $this->username;
	}
}
