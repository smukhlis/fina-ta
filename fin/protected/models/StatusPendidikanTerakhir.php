<?php

/**
 * This is the model class for table "status_pendidikan_terakhir".
 *
 * The followings are the available columns in table 'status_pendidikan_terakhir':
 * @property integer $id
 * @property string $status_pendidikan_terakhir
 * @property string $kategori
 *
 * The followings are the available model relations:
 * @property Guru[] $gurus
 */
class StatusPendidikanTerakhir extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatusPendidikanTerakhir the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'status_pendidikan_terakhir';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'status_pendidikan_terakhir, kategori','length','max' => 255
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, status_pendidikan_terakhir, kategori','safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gurus' => array(
				self::HAS_MANY,'Guru','nilai_status_pandidikan_terakhir_id'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID','status_pendidikan_terakhir' => 'Status Pendidikan Terakhir','kategori' => 'Kategori',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('status_pendidikan_terakhir', $this->status_pendidikan_terakhir, true);
		$criteria->compare('kategori', $this->kategori, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
