<?php

/**
 * This is the model class for table "county".
 *
 * The followings are the available columns in table 'county':
 * @property integer $id
 * @property string $name
 * @property string $name_other
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property City[] $cities
 * @property Loc[] $locs
 * @property Street[] $streets
 */
class County extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return County the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'county';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'status','numerical','integerOnly' => true
				),array(
					'name, name_other','length','max' => 255
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, name_other, status','safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'cities' => array(
					self::HAS_MANY,'City','countyid'
				),'locs' => array(
					self::HAS_MANY,'Loc','countyid'
				),'streets' => array(
					self::HAS_MANY,'Street','contyid'
				),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID','name' => 'Name','name_other' => 'Name Other','status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('name_other', $this->name_other, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
