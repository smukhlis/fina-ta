<?php

/**
 * This is the model class for table "usr".
 *
 * The followings are the available columns in table 'usr':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $privilege
 * @property integer $userid
 * @property integer $status
 */
class UsrPwd extends Usr {
	public $oldPassword;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usr the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}
	public function attributeLabels() {
		return array(
			'password' => 'New Password',
		);
	}

	public function rules() {
		return array(
			array(
				'password, oldPassword','required'
			),array(
				'password, oldPassword','length','max' => 255
			),
		);
	}

}
