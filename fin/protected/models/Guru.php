<?php

/**
 * This is the model class for table "guru".
 *
 * The followings are the available columns in table 'guru':
 * @property integer $id
 * @property string $nama
 * @property string $alamat
 * @property string $no_telp
 * @property integer $nilai_kompetensi_padagogig_id
 * @property integer $nilai_kompetensi_pengembangan_guru_id
 * @property integer $nilai_kepribadian_dan_sosial_guru_id
 * @property integer $nilai_masa_jabatan_id
 * @property integer $nilai_status_pandidikan_terakhir_id
 * @property integer $nilai_prestasi_id
 *
 * The followings are the available model relations:
 * @property StatusPendidikanTerakhir $nilaiStatusPandidikanTerakhir
 * @property NilaiKompetensiPadagogig $nilaiKompetensiPadagogig
 * @property NilaiKepribadianDanSosialGuru $nilaiKompetensiPengembanganGuru
 * @property NilaiKepribadianDanSosialGuru $nilaiKepribadianDanSosialGuru
 * @property NilaiMasaJabatan $nilaiMasaJabatan
 */
class Guru extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Guru the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'guru';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
						'nilai_kompetensi_padagogig_id, nilai_kompetensi_pengembangan_guru_id, nilai_kepribadian_dan_sosial_guru_id, nilai_masa_jabatan_id, nilai_status_pandidikan_terakhir_id, nilai_prestasi_id',
						'numerical','integerOnly' => true
				),array(
					'nama, alamat, no_telp','length','max' => 255
				//),
				),array(
					//'no_telp', 'match', 'pattern'=>'/^([0-9])+$/'
					//'no_telp', 'match', 'pattern'=>'/^[0-9]{1,45}$/'
					'no_telp', 'match', 'pattern'=>'/^[0-9]+$/'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
						'id, nama, alamat, no_telp, nilai_kompetensi_padagogig_id, nilai_kompetensi_pengembangan_guru_id, nilai_kepribadian_dan_sosial_guru_id, nilai_masa_jabatan_id, nilai_status_pandidikan_terakhir_id, nilai_prestasi_id',
						'safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'nilaiStatusPandidikanTerakhir' => array(
					self::BELONGS_TO,'StatusPendidikanTerakhir','nilai_status_pandidikan_terakhir_id'
				),
				'nilaiKompetensiPadagogig' => array(
					self::BELONGS_TO,'NilaiKompetensiPadagogig','nilai_kompetensi_padagogig_id'
				),
				'nilaiKompetensiPengembanganGuru' => array(
					self::BELONGS_TO,'NilaiKompetensiPengembanganGuru','nilai_kompetensi_pengembangan_guru_id'
				),
				'nilaiKepribadianDanSosialGuru' => array(
					self::BELONGS_TO,'NilaiKepribadianDanSosialGuru','nilai_kepribadian_dan_sosial_guru_id'
				),'nilaiMasaJabatan' => array(
					self::BELONGS_TO,'NilaiMasaJabatan','nilai_masa_jabatan_id'
				),'nilaiPrestasi' => array(
					self::BELONGS_TO,'NilaiPrestasi','nilai_prestasi_id'
				),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
				'id' => 'ID','nama' => 'Nama','alamat' => 'Alamat','no_telp' => 'No Telp',
				'nilai_kompetensi_padagogig_id' => 'Nilai Kompetensi Padagogig',
				'nilai_kompetensi_pengembangan_guru_id' => 'Nilai Kompetensi Pengembangan Guru',
				'nilai_kepribadian_dan_sosial_guru_id' => 'Nilai Kepribadian Dan Sosial Guru',
				'nilai_masa_jabatan_id' => 'Nilai Masa Jabatan',
				'nilai_status_pandidikan_terakhir_id' => 'Nilai Status Pandidikan Terakhir',
				'nilai_prestasi_id' => 'Nilai Prestasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('alamat', $this->alamat, true);
		$criteria->compare('no_telp', $this->no_telp, true);
		$criteria->compare('nilai_kompetensi_padagogig_id', $this->nilai_kompetensi_padagogig_id);
		$criteria->compare('nilai_kompetensi_pengembangan_guru_id', $this->nilai_kompetensi_pengembangan_guru_id);
		$criteria->compare('nilai_kepribadian_dan_sosial_guru_id', $this->nilai_kepribadian_dan_sosial_guru_id);
		$criteria->compare('nilai_masa_jabatan_id', $this->nilai_masa_jabatan_id);
		$criteria->compare('nilai_status_pandidikan_terakhir_id', $this->nilai_status_pandidikan_terakhir_id);
		$criteria->compare('nilai_prestasi_id', $this->nilai_prestasi_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
