<?php

/**
 * This is the model class for table "loc".
 *
 * The followings are the available columns in table 'loc':
 * @property integer $id
 * @property string $name
 * @property string $name_other
 * @property integer $cityid
 * @property integer $countyid
 * @property integer $streetid
 * @property integer $counter
 * @property string $description
 * @property string $promo
 *
 * The followings are the available model relations:
 * @property LocNote $id0
 * @property City $city
 * @property County $county
 * @property Street $street
 */
class Loc extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Loc the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'loc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'cityid, countyid, streetid, counter','numerical','integerOnly' => true
				),array(
					'name, name_other, description','length','max' => 255
				),array(
					'promo','safe'
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
						'id, name, name_other, cityid, countyid, streetid, counter, description, promo','safe',
						'on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'id0' => array(
					self::BELONGS_TO,'LocNote','id'
				),'city' => array(
					self::BELONGS_TO,'City','cityid'
				),'county' => array(
					self::BELONGS_TO,'County','countyid'
				),'street' => array(
					self::BELONGS_TO,'Street','streetid'
				),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
				'id' => 'ID','name' => 'Name','name_other' => 'Name Other','cityid' => 'Cityid',
				'countyid' => 'Countyid','streetid' => 'Streetid','counter' => 'Counter',
				'description' => 'Description','promo' => 'Promo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('name_other', $this->name_other, true);
		$criteria->compare('cityid', $this->cityid);
		$criteria->compare('countyid', $this->countyid);
		$criteria->compare('streetid', $this->streetid);
		$criteria->compare('counter', $this->counter);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('promo', $this->promo, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
