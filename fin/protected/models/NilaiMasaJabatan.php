<?php

/**
 * This is the model class for table "nilai_masa_jabatan".
 *
 * The followings are the available columns in table 'nilai_masa_jabatan':
 * @property integer $id
 * @property string $masa_jabatan
 * @property string $kategori
 *
 * The followings are the available model relations:
 * @property Guru[] $gurus
 */
class NilaiMasaJabatan extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NilaiMasaJabatan the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'nilai_masa_jabatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'masa_jabatan, kategori','length','max' => 255
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, masa_jabatan, kategori','safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gurus' => array(
				self::HAS_MANY,'Guru','nilai_masa_jabatan_id'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID','masa_jabatan' => 'Masa Jabatan','kategori' => 'Kategori',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('masa_jabatan', $this->masa_jabatan, true);
		$criteria->compare('kategori', $this->kategori, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
