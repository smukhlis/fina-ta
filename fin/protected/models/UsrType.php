<?php

/**
 * This is the model class for table "usr_type".
 *
 * The followings are the available columns in table 'usr_type':
 * @property integer $id
 * @property string $name
 * @property string $note
 *
 * The followings are the available model relations:
 * @property Usr[] $usrs
 */
class UsrType extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsrType the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'usr_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				array(
					'name','length','max' => 50
				),array(
					'note','length','max' => 255
				),
				// The following rule is used by search().
				// Please remove those attributes that should not be searched.
				array(
					'id, name, note','safe','on' => 'search'
				),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usrs' => array(
				self::HAS_MANY,'Usr','type'
			),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID','name' => 'Name','note' => 'Note',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('note', $this->note, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
