<?php
require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'SimpleImage.php');
class Thumbnailer extends SimpleImage {
	public function create($image, $width, $height = 0) {
		$info = pathinfo($image);
		$file = $info['basename'];
		$dir = $info['dirname'];
		chmod($dir, 0777);
		if (empty($height)) {
			$sizes = getimagesize($image);
			$ratio = $width / $sizes[0];

			$height = intval($ratio * $sizes[1]);
			/**/

		}
		$tmb = $dir . '/.tmb/';
		if (!file_exists($tmb))
			mkdir($tmb, 0777);
		$tmbDir = $tmb . $width . 'x' . $height;
		if (!file_exists($tmbDir))
			mkdir($tmbDir, 0777);

		if (file_exists($tmbDir . '/' . $file))
			return $tmbDir . '/' . $file;
		$this->load($image);
		if (empty($height))
			$this->resizeToWidth($width);
		elseif (empty($width))
			$this->resizeToHeight($height);
		else
			$this->resizeToBoth($width, $height);

		$this->save($tmbDir . '/' . $file);
		return $tmbDir . '/' . $file;
	}
	public function createThumbFromUrl($image, $width, $height = 0) {
		$image = trim($image);
		$host = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;
		$image = str_replace(Yii::app()->request->hostInfo, '', $image);
		$image = str_replace(Yii::app()->request->baseUrl, '', $image);
		$path_root = Yii::getPathOfAlias('webroot');
		$path_image = $path_root . $image;
		if (file_exists($path_image)) {
			return str_replace(Yii::app()->urlManager->urlSuffix, '',
					Yii::app()->controller
							->createUrl(str_replace($path_root, '', $this->create($path_image, $width, $height))));
		} else {
			return $path_image;
		}

	}
}
