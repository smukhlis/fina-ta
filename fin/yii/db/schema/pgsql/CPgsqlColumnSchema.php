<?php

class CPgsqlColumnSchema extends CDbColumnSchema {

	protected function extractType($dbType) {
		if (strpos($dbType, '[') !== false || strpos($dbType, 'char') !== false || strpos($dbType, 'text') !== false)
			$this->type = 'string';
		elseif (strpos($dbType, 'bool') !== false)
			$this->type = 'boolean';
		elseif (preg_match('/(real|float|double)/', $dbType))
			$this->type = 'double';
		elseif (preg_match('/(integer|oid|serial|smallint)/', $dbType))
			$this->type = 'integer';
		else
			$this->type = 'string';
	}

	protected function extractDefault($defaultValue) {
		if ($defaultValue === 'true')
			$this->defaultValue = true;
		elseif ($defaultValue === 'false')
			$this->defaultValue = false;
		elseif (strpos($defaultValue, 'nextval') === 0)
			$this->defaultValue = null;
		elseif (preg_match('/^\'(.*)\'::/', $defaultValue, $matches))
			$this->defaultValue = $this->typecast(str_replace("''", "'", $matches[1]));
		elseif (preg_match('/^-?\d+(\.\d*)?$/', $defaultValue, $matches))
			$this->defaultValue = $this->typecast($defaultValue);
	}
}
