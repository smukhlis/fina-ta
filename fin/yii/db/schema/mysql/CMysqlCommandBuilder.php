<?php

class CMysqlCommandBuilder extends CDbCommandBuilder {

	public function applyJoin($sql, $join) {
		if ($join == '')
			return $sql;

		if (strpos($sql, 'UPDATE') === 0 && ($pos = strpos($sql, 'SET')) !== false)
			return substr($sql, 0, $pos) . $join . ' ' . substr($sql, $pos);
		else
			return $sql . ' ' . $join;
	}
}
