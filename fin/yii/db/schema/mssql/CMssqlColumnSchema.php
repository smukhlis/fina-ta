<?php

class CMssqlColumnSchema extends CDbColumnSchema {

	public function init($dbType, $defaultValue) {
		if ($defaultValue == '(NULL)') {
			$defaultValue = null;
		}
		parent::init($dbType, $defaultValue);
	}

	protected function extractType($dbType) {
		if (strpos($dbType, 'float') !== false || strpos($dbType, 'real') !== false)
			$this->type = 'double';
		elseif (strpos($dbType, 'bigint') === false
				&& (strpos($dbType, 'int') !== false || strpos($dbType, 'smallint') !== false
						|| strpos($dbType, 'tinyint')))
			$this->type = 'integer';
		elseif (strpos($dbType, 'bit') !== false)
			$this->type = 'boolean';
		else
			$this->type = 'string';
	}

	protected function extractDefault($defaultValue) {
		if ($this->dbType === 'timestamp')
			$this->defaultValue = null;
		else
			parent::extractDefault(str_replace(array(
				'(',')',"'"
			), '', $defaultValue));
	}

	protected function extractLimit($dbType) {
	}

	public function typecast($value) {
		if ($this->type === 'boolean')
			return $value ? 1 : 0;
		else
			return parent::typecast($value);
	}
}
