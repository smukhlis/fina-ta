<?php

class CMssqlPdoAdapter extends PDO {

	public function lastInsertId($sequence = NULL) {
		return $this->query('SELECT CAST(COALESCE(SCOPE_IDENTITY(), @@IDENTITY) AS bigint)')->fetchColumn();
	}

	public function beginTransaction() {
		$this->exec('BEGIN TRANSACTION');
		return true;
	}

	public function commit() {
		$this->exec('COMMIT TRANSACTION');
		return true;
	}

	public function rollBack() {
		$this->exec('ROLLBACK TRANSACTION');
		return true;
	}
}
