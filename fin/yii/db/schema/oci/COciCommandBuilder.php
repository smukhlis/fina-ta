<?php

class COciCommandBuilder extends CDbCommandBuilder {

	public $returnID;

	public function getLastInsertID($table) {
		return $this->returnID;
	}

	public function applyLimit($sql, $limit, $offset) {
		if (($limit < 0) and ($offset < 0))
			return $sql;

		$filters = array();
		if ($offset > 0) {
			$filters[] = 'rowNumId > ' . (int) $offset;
		}

		if ($limit >= 0) {
			$filters[] = 'rownum <= ' . (int) $limit;
		}

		if (count($filters) > 0) {
			$filter = implode(' and ', $filters);
			$filter = " WHERE " . $filter;
		} else {
			$filter = '';
		}

		$sql = <<<EOD
WITH USER_SQL AS ({$sql}),
	PAGINATION AS (SELECT USER_SQL.*, rownum as rowNumId FROM USER_SQL)
SELECT *
FROM PAGINATION
		{$filter}
EOD;
		return $sql;
	}

	public function createInsertCommand($table, $data) {
		$this->ensureTable($table);
		$fields = array();
		$values = array();
		$placeholders = array();
		$i = 0;
		foreach ($data as $name => $value) {
			if (($column = $table->getColumn($name)) !== null && ($value !== null || $column->allowNull)) {
				$fields[] = $column->rawName;
				if ($value instanceof CDbExpression) {
					$placeholders[] = $value->expression;
					foreach ($value->params as $n => $v)
						$values[$n] = $v;
				} else {
					$placeholders[] = self::PARAM_PREFIX . $i;
					$values[self::PARAM_PREFIX . $i] = $column->typecast($value);
					$i++;
				}
			}
		}

		$sql = "INSERT INTO {$table->rawName} (" . implode(', ', $fields) . ') VALUES (' . implode(', ', $placeholders)
				. ')';

		if (is_string($table->primaryKey) && ($column = $table->getColumn($table->primaryKey)) !== null
				&& $column->type !== 'string') {
			$sql .= ' RETURNING ' . $column->rawName . ' INTO :RETURN_ID';
			$command = $this->getDbConnection()->createCommand($sql);
			$command->bindParam(':RETURN_ID', $this->returnID, PDO::PARAM_INT, 12);
			$table->sequenceName = 'RETURN_ID';
		} else
			$command = $this->getDbConnection()->createCommand($sql);

		foreach ($values as $name => $value)
			$command->bindValue($name, $value);

		return $command;
	}
}
