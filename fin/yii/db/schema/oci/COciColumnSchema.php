<?php

class COciColumnSchema extends CDbColumnSchema {

	protected function extractOraType($dbType) {
		if (strpos($dbType, 'FLOAT') !== false)
			return 'double';

		if (strpos($dbType, 'NUMBER') !== false || strpos($dbType, 'INTEGER') !== false) {
			if (strpos($dbType, '(') && preg_match('/\((.*)\)/', $dbType, $matches)) {
				$values = explode(',', $matches[1]);
				if (isset($values[1]) and (((int) $values[1]) > 0))
					return 'double';
				else
					return 'integer';
			} else
				return 'double';
		} else
			return 'string';
	}

	protected function extractType($dbType) {
		$this->type = $this->extractOraType($dbType);
	}

	protected function extractDefault($defaultValue) {
		if (stripos($defaultValue, 'timestamp') !== false)
			$this->defaultValue = null;
		else
			parent::extractDefault($defaultValue);
	}
}
