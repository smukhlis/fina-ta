<?php

class CSqliteCommandBuilder extends CDbCommandBuilder {

	protected function createCompositeInCondition($table, $values, $prefix) {
		$keyNames = array();
		foreach (array_keys($values[0]) as $name)
			$keyNames[] = $prefix . $table->columns[$name]->rawName;
		$vs = array();
		foreach ($values as $value)
			$vs[] = implode("||','||", $value);
		return implode("||','||", $keyNames) . ' IN (' . implode(', ', $vs) . ')';
	}
}
