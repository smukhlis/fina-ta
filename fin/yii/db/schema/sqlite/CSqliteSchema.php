<?php

class CSqliteSchema extends CDbSchema {

	public $columnTypes = array(
			'pk' => 'integer PRIMARY KEY AUTOINCREMENT NOT NULL','string' => 'varchar(255)','text' => 'text',
			'integer' => 'integer','float' => 'float','decimal' => 'decimal','datetime' => 'datetime',
			'timestamp' => 'timestamp','time' => 'time','date' => 'date','binary' => 'blob','boolean' => 'tinyint(1)',
			'money' => 'decimal(19,4)',
	);

	public function resetSequence($table, $value = null) {
		if ($table->sequenceName !== null) {
			if ($value === null)
				$value = $this->getDbConnection()
						->createCommand("SELECT MAX(`{$table->primaryKey}`) FROM {$table->rawName}")->queryScalar();
			else
				$value = (int) $value - 1;
			try {
				$this->getDbConnection()
						->createCommand("UPDATE sqlite_sequence SET seq='$value' WHERE name='{$table->name}'")
						->execute();
			} catch (Exception $e) {
			}
		}
	}

	public function checkIntegrity($check = true, $schema = '') {
		return;
	}

	protected function findTableNames($schema = '') {
		$sql = "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name<>'sqlite_sequence'";
		return $this->getDbConnection()->createCommand($sql)->queryColumn();
	}

	protected function createCommandBuilder() {
		return new CSqliteCommandBuilder($this);
	}

	protected function loadTable($name) {
		$table = new CDbTableSchema;
		$table->name = $name;
		$table->rawName = $this->quoteTableName($name);

		if ($this->findColumns($table)) {
			$this->findConstraints($table);
			return $table;
		} else
			return null;
	}

	protected function findColumns($table) {
		$sql = "PRAGMA table_info({$table->rawName})";
		$columns = $this->getDbConnection()->createCommand($sql)->queryAll();
		if (empty($columns))
			return false;

		foreach ($columns as $column) {
			$c = $this->createColumn($column);
			$table->columns[$c->name] = $c;
			if ($c->isPrimaryKey) {
				if ($table->primaryKey === null)
					$table->primaryKey = $c->name;
				elseif (is_string($table->primaryKey))
					$table->primaryKey = array(
						$table->primaryKey,$c->name
					);
				else
					$table->primaryKey[] = $c->name;
			}
		}
		if (is_string($table->primaryKey) && !strncasecmp($table->columns[$table->primaryKey]->dbType, 'int', 3)) {
			$table->sequenceName = '';
			$table->columns[$table->primaryKey]->autoIncrement = true;
		}

		return true;
	}

	protected function findConstraints($table) {
		$foreignKeys = array();
		$sql = "PRAGMA foreign_key_list({$table->rawName})";
		$keys = $this->getDbConnection()->createCommand($sql)->queryAll();
		foreach ($keys as $key) {
			$column = $table->columns[$key['from']];
			$column->isForeignKey = true;
			$foreignKeys[$key['from']] = array(
				$key['table'],$key['to']
			);
		}
		$table->foreignKeys = $foreignKeys;
	}

	protected function createColumn($column) {
		$c = new CSqliteColumnSchema;
		$c->name = $column['name'];
		$c->rawName = $this->quoteColumnName($c->name);
		$c->allowNull = !$column['notnull'];
		$c->isPrimaryKey = $column['pk'] != 0;
		$c->isForeignKey = false;
		$c->comment = null;
		$c->init(strtolower($column['type']), $column['dflt_value']);
		return $c;
	}

	public function renameTable($table, $newName) {
		return 'ALTER TABLE ' . $this->quoteTableName($table) . ' RENAME TO ' . $this->quoteTableName($newName);
	}

	public function truncateTable($table) {
		return "DELETE FROM " . $this->quoteTableName($table);
	}

	public function dropColumn($table, $column) {
		throw new CDbException(Yii::t('yii', 'Dropping DB column is not supported by SQLite.'));
	}

	public function renameColumn($table, $name, $newName) {
		throw new CDbException(Yii::t('yii', 'Renaming a DB column is not supported by SQLite.'));
	}

	public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null) {
		throw new CDbException(
				Yii::t('yii', 'Adding a foreign key constraint to an existing table is not supported by SQLite.'));
	}

	public function dropForeignKey($name, $table) {
		throw new CDbException(Yii::t('yii', 'Dropping a foreign key constraint is not supported by SQLite.'));
	}

	public function alterColumn($table, $column, $type) {
		throw new CDbException(Yii::t('yii', 'Altering a DB column is not supported by SQLite.'));
	}

	public function dropIndex($name, $table) {
		return 'DROP INDEX ' . $this->quoteTableName($name);
	}

	public function addPrimaryKey($name, $table, $columns) {
		throw new CDbException(
				Yii::t('yii', 'Adding a primary key after table has been created is not supported by SQLite.'));
	}

	public function dropPrimaryKey($name, $table) {
		throw new CDbException(
				Yii::t('yii', 'Removing a primary key after table has been created is not supported by SQLite.'));

	}
}
