<?php

class CSqliteColumnSchema extends CDbColumnSchema {

	protected function extractDefault($defaultValue) {
		$this->defaultValue = $this->typecast(strcasecmp($defaultValue, 'null') ? $defaultValue : null);

		if ($this->type === 'string' && $this->defaultValue !== null)
			$this->defaultValue = trim($this->defaultValue, "'\"");
	}
}
