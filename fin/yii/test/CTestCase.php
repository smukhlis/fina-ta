<?php

require_once('PHPUnit/Runner/Version.php');
require_once('PHPUnit/Util/Filesystem.php');
spl_autoload_unregister(array(
	'YiiBase','autoload'
));
require_once('PHPUnit/Autoload.php');
spl_autoload_register(array(
	'YiiBase','autoload'
));
if (in_array('phpunit_autoload', spl_autoload_functions())) {
	spl_autoload_unregister('phpunit_autoload');
	Yii::registerAutoloader('phpunit_autoload');
}

abstract class CTestCase extends PHPUnit_Framework_TestCase {
}
