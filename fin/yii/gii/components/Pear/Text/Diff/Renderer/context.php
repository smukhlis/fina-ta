<?php

require_once 'Text/Diff/Renderer.php';

class Text_Diff_Renderer_context extends Text_Diff_Renderer {

	var $_leading_context_lines = 4;

	var $_trailing_context_lines = 4;

	var $_second_block = '';

	function _blockHeader($xbeg, $xlen, $ybeg, $ylen) {
		if ($xlen != 1) {
			$xbeg .= ',' . $xlen;
		}
		if ($ylen != 1) {
			$ybeg .= ',' . $ylen;
		}
		$this->_second_block = "--- $ybeg ----\n";
		return "***************\n*** $xbeg ****";
	}

	function _endBlock() {
		return $this->_second_block;
	}

	function _context($lines) {
		$this->_second_block .= $this->_lines($lines, '  ');
		return $this->_lines($lines, '  ');
	}

	function _added($lines) {
		$this->_second_block .= $this->_lines($lines, '+ ');
		return '';
	}

	function _deleted($lines) {
		return $this->_lines($lines, '- ');
	}

	function _changed($orig, $final) {
		$this->_second_block .= $this->_lines($final, '! ');
		return $this->_lines($orig, '! ');
	}

}
