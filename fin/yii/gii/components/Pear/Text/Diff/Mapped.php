<?php

class Text_Diff_Mapped extends Text_Diff {

	function Text_Diff_Mapped($from_lines, $to_lines, $mapped_from_lines, $mapped_to_lines) {
		assert(count($from_lines) == count($mapped_from_lines));
		assert(count($to_lines) == count($mapped_to_lines));

		parent::Text_Diff($mapped_from_lines, $mapped_to_lines);

		$xi = $yi = 0;
		for ($i = 0; $i < count($this->_edits); $i++) {
			$orig = &$this->_edits[$i]->orig;
			if (is_array($orig)) {
				$orig = array_slice($from_lines, $xi, count($orig));
				$xi += count($orig);
			}

			$final = &$this->_edits[$i]->final;
			if (is_array($final)) {
				$final = array_slice($to_lines, $yi, count($final));
				$yi += count($final);
			}
		}
	}

}
