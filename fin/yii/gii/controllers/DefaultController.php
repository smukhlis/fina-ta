<?php

class DefaultController extends CController {
	public $layout = '/layouts/column1';

	public function getPageTitle() {
		if ($this->action->id === 'index')
			return 'Gii: a Web-based code generator for Yii';
		else
			return 'Gii - ' . ucfirst($this->action->id) . ' Generator';
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogin() {
		$model = Yii::createComponent('gii.models.LoginForm');

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->createUrl('gii/default/index'));
		}
		$this->render('login', array(
					'model' => $model
				));
	}

	public function actionLogout() {
		Yii::app()->user->logout(false);
		$this->redirect(Yii::app()->createUrl('gii/default/index'));
	}
}
