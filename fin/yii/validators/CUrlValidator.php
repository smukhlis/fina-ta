<?php

class CUrlValidator extends CValidator {

	public $pattern = '/^{schemes}:\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i';

	public $validSchemes = array(
		'http','https'
	);

	public $defaultScheme;

	public $allowEmpty = true;

	public $validateIDN = false;

	protected function validateAttribute($object, $attribute) {
		$value = $object->$attribute;
		if ($this->allowEmpty && $this->isEmpty($value))
			return;
		if (($value = $this->validateValue($value)) !== false)
			$object->$attribute = $value;
		else {
			$message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} is not a valid URL.');
			$this->addError($object, $attribute, $message);
		}
	}

	public function validateValue($value) {
		if (is_string($value) && strlen($value) < 2000) {
			if ($this->validateIDN)
				$value = $this->encodeIDN($value);

			if ($this->defaultScheme !== null && strpos($value, '://') === false)
				$value = $this->defaultScheme . '://' . $value;

			if (strpos($this->pattern, '{schemes}') !== false)
				$pattern = str_replace('{schemes}', '(' . implode('|', $this->validSchemes) . ')', $this->pattern);
			else
				$pattern = $this->pattern;

			if (preg_match($pattern, $value))
				return $this->validateIDN ? $this->decodeIDN($value) : $value;
		}
		return false;
	}

	public function clientValidateAttribute($object, $attribute) {
		if ($this->validateIDN) {
			Yii::app()->getClientScript()->registerCoreScript('punycode');
			$validateIDN = '
var info = value.match(/^(.+:\/\/|)([^/]+)/);
if (info)
	value = info[1] + punycode.toASCII(info[2]);
';
		} else
			$validateIDN = '';

		$message = $this->message !== null ? $this->message : Yii::t('yii', '{attribute} is not a valid URL.');
		$message = strtr($message, array(
			'{attribute}' => $object->getAttributeLabel($attribute),
		));

		if (strpos($this->pattern, '{schemes}') !== false)
			$pattern = str_replace('{schemes}', '(' . implode('|', $this->validSchemes) . ')', $this->pattern);
		else
			$pattern = $this->pattern;

		$js = "
		$validateIDN
if(!value.match($pattern)) {
	messages.push(" . CJSON::encode($message) . ");
}
";
		if ($this->defaultScheme !== null) {
			$js = "
if(!value.match(/:\\/\\//)) {
	value=" . CJSON::encode($this->defaultScheme) . "+'://'+value;
}
					$js
					";
		}

		if ($this->allowEmpty) {
			$js = "
if(jQuery.trim(value)!='') {
	$js
}
			";
		}

		return $js;
	}

	private function encodeIDN($value) {
		require_once(Yii::getPathOfAlias('system.vendors.idna_convert') . DIRECTORY_SEPARATOR
				. 'idna_convert.class.php');
		$idnaConvert = new idna_convert();
		return $idnaConvert->encode($value);
	}

	private function decodeIDN($value) {
		require_once(Yii::getPathOfAlias('system.vendors.idna_convert') . DIRECTORY_SEPARATOR
				. 'idna_convert.class.php');
		$idnaConvert = new idna_convert();
		return $idnaConvert->decode($value);
	}
}
