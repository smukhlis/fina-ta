<?php

class CCaptcha extends CWidget {

	public $captchaAction = 'captcha';

	public $showRefreshButton = true;

	public $clickableImage = false;

	public $buttonLabel;

	public $buttonType = 'link';

	public $imageOptions = array();

	public $buttonOptions = array();

	public function run() {
		if (self::checkRequirements('imagick') || self::checkRequirements('gd')) {
			$this->renderImage();
			$this->registerClientScript();
		} else
			throw new CException(Yii::t('yii', 'GD with FreeType or ImageMagick PHP extensions are required.'));
	}

	protected function renderImage() {
		if (!isset($this->imageOptions['id']))
			$this->imageOptions['id'] = $this->getId();

		$url = $this->getController()->createUrl($this->captchaAction, array(
					'v' => uniqid()
				));
		$alt = isset($this->imageOptions['alt']) ? $this->imageOptions['alt'] : '';
		echo CHtml::image($url, $alt, $this->imageOptions);
	}

	public function registerClientScript() {
		$cs = Yii::app()->clientScript;
		$id = $this->imageOptions['id'];
		$url = $this->getController()->createUrl($this->captchaAction, array(
					CCaptchaAction::REFRESH_GET_VAR => true
				));

		$js = "";
		if ($this->showRefreshButton) {
			$cs->registerScript('Yii.CCaptcha#' . $id, '// dummy');
			$label = $this->buttonLabel === null ? Yii::t('yii', 'Get a new code') : $this->buttonLabel;
			$options = $this->buttonOptions;
			if (isset($options['id']))
				$buttonID = $options['id'];
			else
				$buttonID = $options['id'] = $id . '_button';
			if ($this->buttonType === 'button')
				$html = CHtml::button($label, $options);
			else
				$html = CHtml::link($label, $url, $options);
			$js = "jQuery('#$id').after(" . CJSON::encode($html) . ");";
			$selector = "#$buttonID";
		}

		if ($this->clickableImage)
			$selector = isset($selector) ? "$selector, #$id" : "#$id";

		if (!isset($selector))
			return;

		$js .= "
jQuery(document).on('click', '$selector', function(){
	jQuery.ajax({
		url: " . CJSON::encode($url)
				. ",
		dataType: 'json',
		cache: false,
		success: function(data) {
			jQuery('#$id').attr('src', data['url']);
			jQuery('body').data('{$this->captchaAction}.hash', [data['hash1'], data['hash2']]);
		}
	});
	return false;
});
				";
		$cs->registerScript('Yii.CCaptcha#' . $id, $js);
	}

	public static function checkRequirements($extension = null) {
		if (extension_loaded('imagick')) {
			$imagick = new Imagick();
			$imagickFormats = $imagick->queryFormats('PNG');
		}
		if (extension_loaded('gd')) {
			$gdInfo = gd_info();
		}
		if ($extension === null) {
			if (isset($imagickFormats) && in_array('PNG', $imagickFormats))
				return true;
			if (isset($gdInfo) && $gdInfo['FreeType Support'])
				return true;
		} elseif ($extension == 'imagick' && isset($imagickFormats) && in_array('PNG', $imagickFormats))
			return true;
		elseif ($extension == 'gd' && isset($gdInfo) && $gdInfo['FreeType Support'])
			return true;
		return false;
	}
}
