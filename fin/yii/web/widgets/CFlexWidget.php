<?php

class CFlexWidget extends CWidget {

	public $name;

	public $baseUrl;

	public $width = '100%';

	public $height = '100%';

	public $quality = 'high';

	public $bgColor = '#FFFFFF';

	public $align = 'middle';

	public $allowScriptAccess = 'sameDomain';

	public $allowFullScreen = false;

	public $altHtmlContent;

	public $enableHistory = true;

	public $flashVars = array();

	public function run() {
		if (empty($this->name))
			throw new CException(Yii::t('yii', 'CFlexWidget.name cannot be empty.'));
		if (empty($this->baseUrl))
			throw new CException(Yii::t('yii', 'CFlexWidget.baseUrl cannot be empty.'));
		if ($this->altHtmlContent === null)
			$this->altHtmlContent = Yii::t('yii',
					'This content requires the <a href="http://www.adobe.com/go/getflash/">Adobe Flash Player</a>.');

		$this->registerClientScript();

		$this->render('flexWidget');
	}

	public function registerClientScript() {
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($this->baseUrl . '/AC_OETags.js');

		if ($this->enableHistory) {
			$cs->registerCssFile($this->baseUrl . '/history/history.css');
			$cs->registerScriptFile($this->baseUrl . '/history/history.js');
		}
	}

	public function getFlashVarsAsString() {
		$params = array();
		foreach ($this->flashVars as $k => $v)
			$params[] = urlencode($k) . '=' . urlencode($v);
		return CJavaScript::quote(implode('&', $params));
	}
}
