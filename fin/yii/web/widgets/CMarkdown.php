<?php

class CMarkdown extends COutputProcessor {

	public $cssFile;

	public $purifyOutput = false;

	private $_parser;

	public function processOutput($output) {
		$output = $this->transform($output);
		if ($this->purifyOutput) {
			$purifier = new CHtmlPurifier;
			$output = $purifier->purify($output);
		}
		parent::processOutput($output);
	}

	public function transform($output) {
		$this->registerClientScript();
		return $this->getMarkdownParser()->transform($output);
	}

	public function registerClientScript() {
		if ($this->cssFile !== false)
			self::registerCssFile($this->cssFile);
	}

	public static function registerCssFile($url = null) {
		CTextHighlighter::registerCssFile($url);
	}

	public function getMarkdownParser() {
		if ($this->_parser === null)
			$this->_parser = $this->createMarkdownParser();
		return $this->_parser;
	}

	protected function createMarkdownParser() {
		return new CMarkdownParser;
	}
}
