<?php

require_once(Yii::getPathOfAlias('system.vendors.TextHighlighter.Text.Highlighter') . '.php');
require_once(Yii::getPathOfAlias('system.vendors.TextHighlighter.Text.Highlighter.Renderer.Html') . '.php');

class CTextHighlighter extends COutputProcessor {

	public $language;

	public $showLineNumbers = false;

	public $lineNumberStyle = 'list';

	public $tabSize = 4;

	public $cssFile;

	public $containerOptions = array();

	public function processOutput($output) {
		$output = $this->highlight($output);
		parent::processOutput($output);
	}

	public function highlight($content) {
		$this->registerClientScript();

		$options['use_language'] = true;
		$options['tabsize'] = $this->tabSize;
		if ($this->showLineNumbers)
			$options['numbers'] = ($this->lineNumberStyle === 'list') ? HL_NUMBERS_LI : HL_NUMBERS_TABLE;

		$highlighter = empty($this->language) ? false : Text_Highlighter::factory($this->language);
		if ($highlighter === false)
			$o = '<pre>' . CHtml::encode($content) . '</pre>';
		else {
			$highlighter->setRenderer(new Text_Highlighter_Renderer_Html($options));
			$o = preg_replace('/<span\s+[^>]*>(\s*)<\/span>/', '\1', $highlighter->highlight($content));
		}

		return CHtml::tag('div', $this->containerOptions, $o);
	}

	public function registerClientScript() {
		if ($this->cssFile !== false)
			self::registerCssFile($this->cssFile);
	}

	public static function registerCssFile($url = null) {
		if ($url === null)
			$url = CHtml::asset(Yii::getPathOfAlias('system.vendors.TextHighlighter.highlight') . '.css');
		Yii::app()->getClientScript()->registerCssFile($url);
	}
}
