<?php

class CDataProviderIterator extends CComponent implements Iterator, Countable {
	private $_dataProvider;
	private $_currentIndex = -1;
	private $_currentPage = 0;
	private $_totalItemCount = -1;
	private $_items;

	public function __construct(CDataProvider $dataProvider, $pageSize = null) {
		$this->_dataProvider = $dataProvider;
		$this->_totalItemCount = $dataProvider->getTotalItemCount();

		if (($pagination = $this->_dataProvider->getPagination()) === false)
			$this->_dataProvider->setPagination(new CPagination());

		if ($pageSize !== null)
			$pagination->setPageSize($pageSize);
	}

	public function getDataProvider() {
		return $this->_dataProvider;
	}

	public function getTotalItemCount() {
		return $this->_totalItemCount;
	}

	protected function loadPage() {
		$this->_dataProvider->getPagination()->setCurrentPage($this->_currentPage);
		return $this->_items = $this->dataProvider->getData(true);
	}

	public function current() {
		return $this->_items[$this->_currentIndex];
	}

	public function key() {
		$pageSize = $this->_dataProvider->getPagination()->getPageSize();
		return $this->_currentPage * $pageSize + $this->_currentIndex;
	}

	public function next() {
		$pageSize = $this->_dataProvider->getPagination()->getPageSize();
		$this->_currentIndex++;
		if ($this->_currentIndex >= $pageSize) {
			$this->_currentPage++;
			$this->_currentIndex = 0;
			$this->loadPage();
		}
	}

	public function rewind() {
		$this->_currentIndex = 0;
		$this->_currentPage = 0;
		$this->loadPage();
	}

	public function valid() {
		return $this->key() < $this->_totalItemCount;
	}

	public function count() {
		return $this->_totalItemCount;
	}
}
