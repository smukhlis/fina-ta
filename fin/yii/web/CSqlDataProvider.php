<?php

class CSqlDataProvider extends CDataProvider {

	public $db;

	public $sql;

	public $params = array();

	public $keyField = 'id';

	public function __construct($sql, $config = array()) {
		$this->sql = $sql;
		foreach ($config as $key => $value)
			$this->$key = $value;
	}

	protected function fetchData() {
		if (!($this->sql instanceof CDbCommand)) {
			$db = $this->db === null ? Yii::app()->db : $this->db;
			$command = $db->createCommand($this->sql);
		} else
			$command = clone $this->sql;

		if (($sort = $this->getSort()) !== false) {
			$order = $sort->getOrderBy();
			if (!empty($order)) {
				if (preg_match('/\s+order\s+by\s+[\w\s,]+$/i', $command->text))
					$command->text .= ', ' . $order;
				else
					$command->text .= ' ORDER BY ' . $order;
			}
		}

		if (($pagination = $this->getPagination()) !== false) {
			$pagination->setItemCount($this->getTotalItemCount());
			$limit = $pagination->getLimit();
			$offset = $pagination->getOffset();
			$command->text = $command->getConnection()->getCommandBuilder()->applyLimit($command->text, $limit, $offset);
		}

		foreach ($this->params as $name => $value)
			$command->bindValue($name, $value);

		return $command->queryAll();
	}

	protected function fetchKeys() {
		$keys = array();
		foreach ($this->getData() as $i => $data)
			$keys[$i] = $data[$this->keyField];
		return $keys;
	}

	protected function calculateTotalItemCount() {
		return 0;
	}
}
