<?php

class CForm extends CFormElement implements ArrayAccess {

	public $title;

	public $description;

	public $method = 'post';

	public $action = '';

	public $inputElementClass = 'CFormInputElement';

	public $buttonElementClass = 'CFormButtonElement';

	public $attributes = array();

	public $showErrorSummary = false;

	public $activeForm = array(
		'class' => 'CActiveForm'
	);

	private $_model;
	private $_elements;
	private $_buttons;
	private $_activeForm;

	public function __construct($config, $model = null, $parent = null) {
		$this->setModel($model);
		if ($parent === null)
			$parent = Yii::app()->getController();
		parent::__construct($config, $parent);
		$this->init();
	}

	protected function init() {
	}

	public function submitted($buttonName = 'submit', $loadData = true) {
		$ret = $this->clicked($this->getUniqueId()) && $this->clicked($buttonName);
		if ($ret && $loadData)
			$this->loadData();
		return $ret;
	}

	public function clicked($name) {
		if (strcasecmp($this->getRoot()->method, 'get'))
			return isset($_POST[$name]);
		else
			return isset($_GET[$name]);
	}

	public function validate() {
		$ret = true;
		foreach ($this->getModels() as $model)
			$ret = $model->validate() && $ret;
		return $ret;
	}

	public function loadData() {
		if ($this->_model !== null) {
			$class = get_class($this->_model);
			if (strcasecmp($this->getRoot()->method, 'get')) {
				if (isset($_POST[$class]))
					$this->_model->setAttributes($_POST[$class]);
			} elseif (isset($_GET[$class]))
				$this->_model->setAttributes($_GET[$class]);
		}
		foreach ($this->getElements() as $element) {
			if ($element instanceof self)
				$element->loadData();
		}
	}

	public function getRoot() {
		$root = $this;
		while ($root->getParent() instanceof self)
			$root = $root->getParent();
		return $root;
	}

	public function getActiveFormWidget() {
		if ($this->_activeForm !== null)
			return $this->_activeForm;
		else
			return $this->getRoot()->_activeForm;
	}

	public function getOwner() {
		$owner = $this->getParent();
		while ($owner instanceof self)
			$owner = $owner->getParent();
		return $owner;
	}

	public function getModel($checkParent = true) {
		if (!$checkParent)
			return $this->_model;
		$form = $this;
		while ($form->_model === null && $form->getParent() instanceof self)
			$form = $form->getParent();
		return $form->_model;
	}

	public function setModel($model) {
		$this->_model = $model;
	}

	public function getModels() {
		$models = array();
		if ($this->_model !== null)
			$models[] = $this->_model;
		foreach ($this->getElements() as $element) {
			if ($element instanceof self)
				$models = array_merge($models, $element->getModels());
		}
		return $models;
	}

	public function getElements() {
		if ($this->_elements === null)
			$this->_elements = new CFormElementCollection($this, false);
		return $this->_elements;
	}

	public function setElements($elements) {
		$collection = $this->getElements();
		foreach ($elements as $name => $config)
			$collection->add($name, $config);
	}

	public function getButtons() {
		if ($this->_buttons === null)
			$this->_buttons = new CFormElementCollection($this, true);
		return $this->_buttons;
	}

	public function setButtons($buttons) {
		$collection = $this->getButtons();
		foreach ($buttons as $name => $config)
			$collection->add($name, $config);
	}

	public function render() {
		return $this->renderBegin() . $this->renderBody() . $this->renderEnd();
	}

	public function renderBegin() {
		if ($this->getParent() instanceof self)
			return '';
		else {
			$options = $this->activeForm;
			if (isset($options['class'])) {
				$class = $options['class'];
				unset($options['class']);
			} else
				$class = 'CActiveForm';
			$options['action'] = $this->action;
			$options['method'] = $this->method;
			if (isset($options['htmlOptions'])) {
				foreach ($this->attributes as $name => $value)
					$options['htmlOptions'][$name] = $value;
			} else
				$options['htmlOptions'] = $this->attributes;
			ob_start();
			$this->_activeForm = $this->getOwner()->beginWidget($class, $options);
			return ob_get_clean() . "<div style=\"visibility:hidden\">" . CHtml::hiddenField($this->getUniqueID(), 1)
					. "</div>\n";
		}
	}

	public function renderEnd() {
		if ($this->getParent() instanceof self)
			return '';
		else {
			ob_start();
			$this->getOwner()->endWidget();
			return ob_get_clean();
		}
	}

	public function renderBody() {
		$output = '';
		if ($this->title !== null) {
			if ($this->getParent() instanceof self) {
				$attributes = $this->attributes;
				unset($attributes['name'], $attributes['type']);
				$output = CHtml::openTag('fieldset', $attributes) . "<legend>" . $this->title . "</legend>\n";
			} else
				$output = "<fieldset>\n<legend>" . $this->title . "</legend>\n";
		}

		if ($this->description !== null)
			$output .= "<div class=\"description\">\n" . $this->description . "</div>\n";

		if ($this->showErrorSummary && ($model = $this->getModel(false)) !== null)
			$output .= $this->getActiveFormWidget()->errorSummary($model) . "\n";

		$output .= $this->renderElements() . "\n" . $this->renderButtons() . "\n";

		if ($this->title !== null)
			$output .= "</fieldset>\n";

		return $output;
	}

	public function renderElements() {
		$output = '';
		foreach ($this->getElements() as $element)
			$output .= $this->renderElement($element);
		return $output;
	}

	public function renderButtons() {
		$output = '';
		foreach ($this->getButtons() as $button)
			$output .= $this->renderElement($button);
		return $output !== '' ? "<div class=\"row buttons\">" . $output . "</div>\n" : '';
	}

	public function renderElement($element) {
		if (is_string($element)) {
			if (($e = $this[$element]) === null && ($e = $this->getButtons()->itemAt($element)) === null)
				return $element;
			else
				$element = $e;
		}
		if ($element->getVisible()) {
			if ($element instanceof CFormInputElement) {
				if ($element->type === 'hidden')
					return "<div style=\"visibility:hidden\">\n" . $element->render() . "</div>\n";
				else
					return "<div class=\"row field_{$element->name}\">\n" . $element->render() . "</div>\n";
			} elseif ($element instanceof CFormButtonElement)
				return $element->render() . "\n";
			else
				return $element->render();
		}
		return '';
	}

	public function addedElement($name, $element, $forButtons) {
	}

	public function removedElement($name, $element, $forButtons) {
	}

	protected function evaluateVisible() {
		foreach ($this->getElements() as $element)
			if ($element->getVisible())
				return true;
		return false;
	}

	protected function getUniqueId() {
		if (isset($this->attributes['id']))
			return 'yform_' . $this->attributes['id'];
		else
			return 'yform_' . sprintf('%x', crc32(serialize(array_keys($this->getElements()->toArray()))));
	}

	public function offsetExists($offset) {
		return $this->getElements()->contains($offset);
	}

	public function offsetGet($offset) {
		return $this->getElements()->itemAt($offset);
	}

	public function offsetSet($offset, $item) {
		$this->getElements()->add($offset, $item);
	}

	public function offsetUnset($offset) {
		$this->getElements()->remove($offset);
	}
}
