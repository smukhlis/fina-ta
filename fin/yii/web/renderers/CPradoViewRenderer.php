<?php

class CPradoViewRenderer extends CViewRenderer {
	private $_input;
	private $_output;
	private $_sourceFile;

	protected function generateViewFile($sourceFile, $viewFile) {
		static $regexRules = array(
				'<%=?\s*(.*?)\s*%>',
				'<\/?(com|cache|clip):([\w\.]+)\s*((?:\s*\w+\s*=\s*\'.*?(?<!\\\\)\'|\s*\w+\s*=\s*".*?(?<!\\\\)"|\s*\w+\s*=\s*\{.*?\})*)\s*\/?>',
				'<!---.*?--->',
		);
		$this->_sourceFile = $sourceFile;
		$this->_input = file_get_contents($sourceFile);
		$n = preg_match_all('/' . implode('|', $regexRules) . '/msS', $this->_input, $matches,
				PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
		$textStart = 0;
		$this->_output = "<?php /* source file: $sourceFile */ ?>\n";
		for ($i = 0; $i < $n; ++$i) {
			$match = &$matches[$i];
			$str = $match[0][0];
			$matchStart = $match[0][1];
			$matchEnd = $matchStart + strlen($str) - 1;

			if ($matchStart > $textStart)
				$this->_output .= substr($this->_input, $textStart, $matchStart - $textStart);
			$textStart = $matchEnd + 1;

			if (strpos($str, '<com:') === 0) {
				$type = $match[3][0];
				if ($str[strlen($str) - 2] !== '/')
					$this->_output .= $this->processBeginWidget($type, $match[4][0], $match[2][1]);
				else
					$this->_output .= $this->processWidget($type, $match[4][0], $match[2][1]);
			} elseif (strpos($str, '</com:') === 0)
				$this->_output .= $this->processEndWidget($match[3][0], $match[2][1]);
			elseif (strpos($str, '<cache:') === 0) {
				$id = $match[3][0];
				if ($str[strlen($str) - 2] !== '/')
					$this->_output .= $this->processBeginCache($id, $match[4][0], $match[2][1]);
				else
					$this->_output .= $this->processCache($id, $match[4][0], $match[2][1]);
			} elseif (strpos($str, '</cache:') === 0)
				$this->_output .= $this->processEndCache($match[3][0], $match[2][1]);
			elseif (strpos($str, '<clip:') === 0) {
				$id = $match[3][0];
				if ($str[strlen($str) - 2] !== '/')
					$this->_output .= $this->processBeginClip($id, $match[4][0], $match[2][1]);
				else
					$this->_output .= $this->processClip($id, $match[4][0], $match[2][1]);
			} elseif (strpos($str, '</clip:') === 0)
				$this->_output .= $this->processEndClip($match[3][0], $match[2][1]);
			elseif (strpos($str, '<%=') === 0)
				$this->_output .= $this->processExpression($match[1][0], $match[1][1]);
			elseif (strpos($str, '<%') === 0)
				$this->_output .= $this->processStatement($match[1][0], $match[1][1]);
		}
		if ($textStart < strlen($this->_input))
			$this->_output .= substr($this->_input, $textStart);

		file_put_contents($viewFile, $this->_output);
	}

	private function processWidget($type, $attributes, $offset) {
		$attrs = $this->processAttributes($attributes);
		if (empty($attrs))
			return $this->generatePhpCode("\$this->widget('$type');", $offset);
		else
			return $this->generatePhpCode("\$this->widget('$type', array($attrs));", $offset);
	}

	private function processBeginWidget($type, $attributes, $offset) {
		$attrs = $this->processAttributes($attributes);
		if (empty($attrs))
			return $this->generatePhpCode("\$this->beginWidget('$type');", $offset);
		else
			return $this->generatePhpCode("\$this->beginWidget('$type', array($attrs));", $offset);
	}

	private function processEndWidget($type, $offset) {
		return $this->generatePhpCode("\$this->endWidget('$type');", $offset);
	}

	private function processCache($id, $attributes, $offset) {
		return $this->processBeginCache($id, $attributes, $offset) . $this->processEndCache($id, $offset);
	}

	private function processBeginCache($id, $attributes, $offset) {
		$attrs = $this->processAttributes($attributes);
		if (empty($attrs))
			return $this->generatePhpCode("if(\$this->beginCache('$id')):", $offset);
		else
			return $this->generatePhpCode("if(\$this->beginCache('$id', array($attrs))):", $offset);
	}

	private function processEndCache($id, $offset) {
		return $this->generatePhpCode("\$this->endCache('$id'); endif;", $offset);
	}

	private function processClip($id, $attributes, $offset) {
		return $this->processBeginClip($id, $attributes, $offset) . $this->processEndClip($id, $offset);
	}

	private function processBeginClip($id, $attributes, $offset) {
		$attrs = $this->processAttributes($attributes);
		if (empty($attrs))
			return $this->generatePhpCode("\$this->beginClip('$id');", $offset);
		else
			return $this->generatePhpCode("\$this->beginClip('$id', array($attrs));", $offset);
	}

	private function processEndClip($id, $offset) {
		return $this->generatePhpCode("\$this->endClip('$id');", $offset);
	}

	private function processExpression($expression, $offset) {
		return $this->generatePhpCode('echo ' . $expression, $offset);
	}

	private function processStatement($statement, $offset) {
		return $this->generatePhpCode($statement, $offset);
	}

	private function generatePhpCode($code, $offset) {
		$line = $this->getLineNumber($offset);
		$code = str_replace('__FILE__', var_export($this->_sourceFile, true), $code);
		return "<?php /* line $line */ $code ?>";
	}

	private function processAttributes($str) {
		static $pattern = '/(\w+)\s*=\s*(\'.*?(?<!\\\\)\'|".*?(?<!\\\\)"|\{.*?\})/msS';
		$attributes = array();
		$n = preg_match_all($pattern, $str, $matches, PREG_SET_ORDER);
		for ($i = 0; $i < $n; ++$i) {
			$match = &$matches[$i];
			$name = $match[1];
			$value = $match[2];
			if ($value[0] === '{')
				$attributes[] = "'$name'=>" . str_replace('__FILE__', $this->_sourceFile, substr($value, 1, -1));
			else
				$attributes[] = "'$name'=>$value";
		}
		return implode(', ', $attributes);
	}

	private function getLineNumber($offset) {
		return count(explode("\n", substr($this->_input, 0, $offset)));
	}
}
