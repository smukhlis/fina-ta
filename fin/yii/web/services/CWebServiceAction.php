<?php

class CWebServiceAction extends CAction {

	public $provider;

	public $serviceUrl;

	public $wsdlUrl;

	public $serviceVar = 'ws';

	public $classMap;

	public $serviceOptions = array();

	private $_service;

	public function run() {
		$hostInfo = Yii::app()->getRequest()->getHostInfo();
		$controller = $this->getController();
		if (($serviceUrl = $this->serviceUrl) === null)
			$serviceUrl = $hostInfo . $controller->createUrl($this->getId(), array(
								$this->serviceVar => 1
							));
		if (($wsdlUrl = $this->wsdlUrl) === null)
			$wsdlUrl = $hostInfo . $controller->createUrl($this->getId());
		if (($provider = $this->provider) === null)
			$provider = $controller;

		$this->_service = $this->createWebService($provider, $wsdlUrl, $serviceUrl);

		if (is_array($this->classMap))
			$this->_service->classMap = $this->classMap;

		foreach ($this->serviceOptions as $name => $value)
			$this->_service->$name = $value;

		if (isset($_GET[$this->serviceVar]))
			$this->_service->run();
		else
			$this->_service->renderWsdl();

		Yii::app()->end();
	}

	public function getService() {
		return $this->_service;
	}

	protected function createWebService($provider, $wsdlUrl, $serviceUrl) {
		return new CWebService($provider, $wsdlUrl, $serviceUrl);
	}
}
