<?php

class CWebService extends CComponent {
	const SOAP_ERROR = 1001;

	public $provider;

	public $wsdlUrl;

	public $serviceUrl;

	public $wsdlCacheDuration = 0;

	public $cacheID = 'cache';

	public $encoding = 'UTF-8';

	public $classMap = array();

	public $actor;

	public $soapVersion;

	public $persistence;

	public $generatorConfig = 'CWsdlGenerator';

	private $_method;

	public function __construct($provider, $wsdlUrl, $serviceUrl) {
		$this->provider = $provider;
		$this->wsdlUrl = $wsdlUrl;
		$this->serviceUrl = $serviceUrl;
	}

	public function handleError($event) {
		$event->handled = true;
		$message = $event->message;
		if (YII_DEBUG) {
			$trace = debug_backtrace();
			if (isset($trace[2]) && isset($trace[2]['file']) && isset($trace[2]['line']))
				$message .= ' (' . $trace[2]['file'] . ':' . $trace[2]['line'] . ')';
		}
		throw new CException($message, self::SOAP_ERROR);
	}

	public function renderWsdl() {
		$wsdl = $this->generateWsdl();
		header('Content-Type: text/xml;charset=' . $this->encoding);
		header('Content-Length: ' . (function_exists('mb_strlen') ? mb_strlen($wsdl, '8bit') : strlen($wsdl)));
		echo $wsdl;
	}

	public function generateWsdl() {
		$providerClass = is_object($this->provider) ? get_class($this->provider) : Yii::import($this->provider, true);
		if ($this->wsdlCacheDuration > 0 && $this->cacheID !== false
				&& ($cache = Yii::app()->getComponent($this->cacheID)) !== null) {
			$key = 'Yii.CWebService.' . $providerClass . $this->serviceUrl . $this->encoding;
			if (($wsdl = $cache->get($key)) !== false)
				return $wsdl;
		}
		$generator = Yii::createComponent($this->generatorConfig);
		$wsdl = $generator->generateWsdl($providerClass, $this->serviceUrl, $this->encoding);
		if (isset($key))
			$cache->set($key, $wsdl, $this->wsdlCacheDuration);
		return $wsdl;
	}

	public function run() {
		header('Content-Type: text/xml;charset=' . $this->encoding);
		if (YII_DEBUG)
			ini_set("soap.wsdl_cache_enabled", 0);
		$server = new SoapServer($this->wsdlUrl, $this->getOptions());
		Yii::app()->attachEventHandler('onError', array(
					$this,'handleError'
				));
		try {
			if ($this->persistence !== null)
				$server->setPersistence($this->persistence);
			if (is_string($this->provider))
				$provider = Yii::createComponent($this->provider);
			else
				$provider = $this->provider;

			if (method_exists($server, 'setObject'))
				$server->setObject($provider);
			else
				$server->setClass('CSoapObjectWrapper', $provider);

			if ($provider instanceof IWebServiceProvider) {
				if ($provider->beforeWebMethod($this)) {
					$server->handle();
					$provider->afterWebMethod($this);
				}
			} else
				$server->handle();
		} catch (Exception $e) {
			if ($e->getCode() !== self::SOAP_ERROR) {
				Yii::log($e->__toString(), CLogger::LEVEL_ERROR, 'application');
			}
			$message = $e->getMessage();
			if (YII_DEBUG)
				$message .= ' (' . $e->getFile() . ':' . $e->getLine() . ")\n" . $e->getTraceAsString();

			Yii::app()->onEndRequest(new CEvent($this));
			$server->fault(get_class($e), $message);
			exit(1);
		}
	}

	public function getMethodName() {
		if ($this->_method === null) {
			if (isset($HTTP_RAW_POST_DATA))
				$request = $HTTP_RAW_POST_DATA;
			else
				$request = file_get_contents('php://input');
			if (preg_match('/<.*?:Body[^>]*>\s*<.*?:(\w+)/mi', $request, $matches))
				$this->_method = $matches[1];
			else
				$this->_method = '';
		}
		return $this->_method;
	}

	protected function getOptions() {
		$options = array();
		if ($this->soapVersion === '1.1')
			$options['soap_version'] = SOAP_1_1;
		elseif ($this->soapVersion === '1.2')
			$options['soap_version'] = SOAP_1_2;
		if ($this->actor !== null)
			$options['actor'] = $this->actor;
		$options['encoding'] = $this->encoding;
		foreach ($this->classMap as $type => $className) {
			$className = Yii::import($className, true);
			if (is_int($type))
				$type = $className;
			$options['classmap'][$type] = $className;
		}
		return $options;
	}
}

class CSoapObjectWrapper {

	public $object = null;

	public function __construct($object) {
		$this->object = $object;
	}

	public function __call($name, $arguments) {
		return call_user_func_array(array(
			$this->object,$name
		), $arguments);
	}
}

