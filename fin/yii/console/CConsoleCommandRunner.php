<?php

class CConsoleCommandRunner extends CComponent {

	public $commands = array();

	private $_scriptName;

	public function run($args) {
		$this->_scriptName = $args[0];
		array_shift($args);
		if (isset($args[0])) {
			$name = $args[0];
			array_shift($args);
		} else
			$name = 'help';

		if (($command = $this->createCommand($name)) === null)
			$command = $this->createCommand('help');
		$command->init();
		return $command->run($args);
	}

	public function getScriptName() {
		return $this->_scriptName;
	}

	public function findCommands($path) {
		if (($dir = @opendir($path)) === false)
			return array();
		$commands = array();
		while (($name = readdir($dir)) !== false) {
			$file = $path . DIRECTORY_SEPARATOR . $name;
			if (!strcasecmp(substr($name, -11), 'Command.php') && is_file($file))
				$commands[strtolower(substr($name, 0, -11))] = $file;
		}
		closedir($dir);
		return $commands;
	}

	public function addCommands($path) {
		if (($commands = $this->findCommands($path)) !== array()) {
			foreach ($commands as $name => $file) {
				if (!isset($this->commands[$name]))
					$this->commands[$name] = $file;
			}
		}
	}

	public function createCommand($name) {
		$name = strtolower($name);

		$command = null;
		if (isset($this->commands[$name]))
			$command = $this->commands[$name];
		else {
			$commands = array_change_key_case($this->commands);
			if (isset($commands[$name]))
				$command = $commands[$name];
		}

		if ($command !== null) {
			if (is_string($command)) {
				if (strpos($command, '/') !== false || strpos($command, '\\') !== false) {
					$className = substr(basename($command), 0, -4);
					if (!class_exists($className, false))
						require_once($command);
				} else
					$className = Yii::import($command);
				return new $className($name, $this);
			} else
				return Yii::createComponent($command, $name, $this);
		} elseif ($name === 'help')
			return new CHelpCommand('help', $this);
		else
			return null;
	}
}
