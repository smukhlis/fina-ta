<?php

class CChainedLogFilter extends CComponent implements ILogFilter {

	public $filters = array();

	public function filter(&$logs) {
		foreach ($this->filters as $filter)
			Yii::createComponent($filter)->filter($logs);
	}
}
