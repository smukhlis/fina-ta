<?php

class CFormatter extends CApplicationComponent {

	private $_htmlPurifier;

	public $dateFormat = 'Y/m/d';

	public $timeFormat = 'h:i:s A';

	public $datetimeFormat = 'Y/m/d h:i:s A';

	public $numberFormat = array(
		'decimals' => null,'decimalSeparator' => null,'thousandSeparator' => null
	);

	public $booleanFormat = array(
		'No','Yes'
	);

	public $htmlPurifierOptions = array();

	public $sizeFormat = array(
		'base' => 1024,'decimals' => 2,'decimalSeparator' => null,
	);

	public function __call($name, $parameters) {
		if (method_exists($this, 'format' . $name))
			return call_user_func_array(array(
				$this,'format' . $name
			), $parameters);
		else
			return parent::__call($name, $parameters);
	}

	public function format($value, $type) {
		$method = 'format' . $type;
		if (method_exists($this, $method))
			return $this->$method($value);
		else
			throw new CException(Yii::t('yii', 'Unknown type "{type}".', array(
				'{type}' => $type
			)));
	}

	public function formatRaw($value) {
		return $value;
	}

	public function formatText($value) {
		return CHtml::encode($value);
	}

	public function formatNtext($value) {
		return nl2br(CHtml::encode($value));
	}

	public function formatHtml($value) {
		return $this->getHtmlPurifier()->purify($value);
	}

	public function formatDate($value) {
		return date($this->dateFormat, $this->normalizeDateValue($value));
	}

	public function formatTime($value) {
		return date($this->timeFormat, $this->normalizeDateValue($value));
	}

	public function formatDatetime($value) {
		return date($this->datetimeFormat, $this->normalizeDateValue($value));
	}

	private function normalizeDateValue($time) {
		if (is_string($time)) {
			if (ctype_digit($time) || ($time{0} == '-' && ctype_digit(substr($time, 1))))
				return (int) $time;
			else
				return strtotime($time);
		}
		return (int) $time;
	}

	public function formatBoolean($value) {
		return $value ? $this->booleanFormat[1] : $this->booleanFormat[0];
	}

	public function formatEmail($value) {
		return CHtml::mailto($value);
	}

	public function formatImage($value) {
		return CHtml::image($value);
	}

	public function formatUrl($value) {
		$url = $value;
		if (strpos($url, 'http://') !== 0 && strpos($url, 'https://') !== 0)
			$url = 'http://' . $url;
		return CHtml::link(CHtml::encode($value), $url);
	}

	public function formatNumber($value) {
		return number_format($value, $this->numberFormat['decimals'], $this->numberFormat['decimalSeparator'],
				$this->numberFormat['thousandSeparator']);
	}

	public function getHtmlPurifier() {
		if ($this->_htmlPurifier === null)
			$this->_htmlPurifier = new CHtmlPurifier;
		$this->_htmlPurifier->options = $this->htmlPurifierOptions;
		return $this->_htmlPurifier;
	}

	public function formatSize($value, $verbose = false) {
		$base = $this->sizeFormat['base'];
		for ($i = 0; $base <= $value && $i < 5; $i++)
			$value = $value / $base;

		$value = round($value, $this->sizeFormat['decimals']);
		$formattedValue = isset($this->sizeFormat['decimalSeparator']) ? str_replace('.',
						$this->sizeFormat['decimalSeparator'], $value) : $value;
		$params = array(
			$value,'{n}' => $formattedValue
		);

		switch ($i) {
			case 0:
				return $verbose ? Yii::t('yii', '{n} byte|{n} bytes', $params) : Yii::t('yii', '{n} B', $params);
			case 1:
				return $verbose ? Yii::t('yii', '{n} kilobyte|{n} kilobytes', $params) : Yii::t('yii', '{n} KB',
						$params);
			case 2:
				return $verbose ? Yii::t('yii', '{n} megabyte|{n} megabytes', $params) : Yii::t('yii', '{n} MB',
						$params);
			case 3:
				return $verbose ? Yii::t('yii', '{n} gigabyte|{n} gigabytes', $params) : Yii::t('yii', '{n} GB',
						$params);
			default:
				return $verbose ? Yii::t('yii', '{n} terabyte|{n} terabytes', $params) : Yii::t('yii', '{n} TB',
						$params);
		}
	}
}
