<?php

return array(
		'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..','name' => 'My Web Application',
		'preload' => array(
			'log'
		),'import' => array(
			'application.models.*','application.components.*',
		),'modules' => array(),
		'components' => array(
				'user' => array(
					'allowAutoLogin' => true,
				),'db' => array(
					'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/testdrive.db',
				),'errorHandler' => array(
					'errorAction' => 'site/error',
				),
				'log' => array(
						'class' => 'CLogRouter',
						'routes' => array(
							array(
								'class' => 'CFileLogRoute','levels' => 'error, warning',
							),
						),
				),
		),'params' => array(
			'adminEmail' => 'webmaster@example.com',
		),
);
