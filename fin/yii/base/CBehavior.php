<?php
class CBehavior extends CComponent implements IBehavior {
	private $_enabled = false;
	private $_owner;
	public function events() {
		return array();
	}
	public function attach($owner) {
		$this->_enabled = true;
		$this->_owner = $owner;
		$this->_attachEventHandlers();
	}
	public function detach($owner) {
		foreach ($this->events() as $event => $handler)
			$owner->detachEventHandler($event, array(
						$this,$handler
					));
		$this->_owner = null;
		$this->_enabled = false;
	}
	public function getOwner() {
		return $this->_owner;
	}
	public function getEnabled() {
		return $this->_enabled;
	}
	public function setEnabled($value) {
		$value = (bool) $value;
		if ($this->_enabled != $value && $this->_owner) {
			if ($value)
				$this->_attachEventHandlers();
			else {
				foreach ($this->events() as $event => $handler)
					$this->_owner->detachEventHandler($event, array(
								$this,$handler
							));
			}
		}
		$this->_enabled = $value;
	}
	private function _attachEventHandlers() {
		$class = new ReflectionClass($this);
		foreach ($this->events() as $event => $handler) {
			if ($class->getMethod($handler)->isPublic())
				$this->_owner->attachEventHandler($event, array(
							$this,$handler
						));
		}
	}
}
