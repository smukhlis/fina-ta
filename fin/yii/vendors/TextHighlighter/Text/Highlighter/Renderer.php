<?php

class Text_Highlighter_Renderer {

	public $_options = array();

	public $_language = '';

	function __construct($options = array()) {
		$this->_options = $options;
	}

	function reset() {
		return;
	}

	function preprocess($str) {
		return $str;
	}

	function acceptToken($class, $content) {
		return;
	}

	function finalize() {
		return;
	}

	function getOutput() {
		return;
	}

	function setCurrentLanguage($lang) {
		$this->_language = $lang;
	}

}

?>
