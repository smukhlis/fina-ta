<?php

require_once dirname(__FILE__) . '/../Renderer.php';
require_once dirname(__FILE__) . '/../Renderer/Array.php';

class Text_Highlighter_Renderer_XML extends Text_Highlighter_Renderer_Array {

	var $_serializer_options = array();

	function reset() {
		parent::reset();
		if (isset($this->_options['xml_serializer'])) {
			$this->_serializer_options = $this->_options['xml_serializer'];
		}
	}

	function finalize() {

		parent::finalize();
		$output = parent::getOutput();

		$serializer = new XML_Serializer($this->_serializer_options);
		$result = $serializer->serialize($output);
		if ($result === true) {
			$this->_output = $serializer->getSerializedData();
		}
	}

}

?>