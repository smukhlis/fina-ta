<?php

require_once dirname(__FILE__) . '/../Renderer.php';

define('HL_CONSOLE_DEFCOLOR', "\033[0m");

class Text_Highlighter_Renderer_Console extends Text_Highlighter_Renderer {

	var $_lastClass;

	var $_numbers = false;

	var $_tabsize = 4;

	var $_output = '';

	var $_colors = array();

	var $_defColors = array(
			'default' => "\033[0m",'inlinetags' => "\033[31m",'brackets' => "\033[36m",'quotes' => "\033[34m",
			'inlinedoc' => "\033[34m",'var' => "\033[1m",'types' => "\033[32m",'number' => "\033[32m",
			'string' => "\033[31m",'reserved' => "\033[35m",'comment' => "\033[33m",'mlcomment' => "\033[33m",
	);

	function preprocess($str) {
		$str = str_replace("\r\n", "\n", $str);
		$str = str_replace("\t", str_repeat(' ', $this->_tabsize), $str);
		return rtrim($str);
	}

	function reset() {
		$this->_lastClass = '';
		if (isset($this->_options['numbers'])) {
			$this->_numbers = (bool) $this->_options['numbers'];
		} else {
			$this->_numbers = false;
		}
		if (isset($this->_options['tabsize'])) {
			$this->_tabsize = $this->_options['tabsize'];
		} else {
			$this->_tabsize = 4;
		}
		if (isset($this->_options['colors'])) {
			$this->_colors = array_merge($this->_defColors, $this->_options['colors']);
		} else {
			$this->_colors = $this->_defColors;
		}
		$this->_output = '';
	}

	function acceptToken($class, $content) {
		if (isset($this->_colors[$class])) {
			$color = $this->_colors[$class];
		} else {
			$color = $this->_colors['default'];
		}
		if ($this->_lastClass != $class) {
			$this->_output .= $color;
		}
		$content = str_replace("\n", $this->_colors['default'] . "\n" . $color, $content);
		$content .= $this->_colors['default'];
		$this->_output .= $content;
	}

	function finalize() {
		if ($this->_numbers) {
			$nlines = substr_count($this->_output, "\n") + 1;
			$len = strlen($nlines);
			$i = 1;
			$this->_output = preg_replace('~^~em', '" " . str_pad($i++, $len, " ", STR_PAD_LEFT) . ": "',
					$this->_output);
		}
		$this->_output .= HL_CONSOLE_DEFCOLOR . "\n";
	}

	function getOutput() {
		return $this->_output;
	}
}

?>
