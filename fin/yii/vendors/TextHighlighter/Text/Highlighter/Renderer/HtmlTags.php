<?php

require_once dirname(__FILE__) . '/../Renderer.php';
require_once dirname(__FILE__) . '/../Renderer/Array.php';

class Text_Highlighter_Renderer_HtmlTags extends Text_Highlighter_Renderer_Array {

	var $_numbers = false;

	var $_hilite_tags = array(
			'default' => '','code' => '','brackets' => 'b','comment' => 'i','mlcomment' => 'i','quotes' => '',
			'string' => 'i','identifier' => 'b','builtin' => 'b','reserved' => 'u','inlinedoc' => 'i',
			'var' => 'b','url' => 'i','special' => '','number' => '','inlinetags' => '',
	);

	function reset() {
		parent::reset();
		if (isset($this->_options['numbers'])) {
			$this->_numbers = $this->_options['numbers'];
		}
		if (isset($this->_options['tags'])) {
			$this->_hilite_tags = array_merge($this->_tags, $this->_options['tags']);
		}
	}

	function finalize() {

		parent::finalize();
		$output = parent::getOutput();

		$html_output = '';

		foreach ($output AS $token) {

			if ($this->_enumerated) {
				$class = $token[0];
				$content = $token[1];
			} else {
				$key = key($token);
				$class = $key;
				$content = $token[$key];
			}

			$iswhitespace = ctype_space($content);
			if (!$iswhitespace && !empty($this->_hilite_tags[$class])) {
				$html_output .= '<' . $this->_hilite_tags[$class] . '>' . $content . '</' . $this->_hilite_tags[$class]
						. '>';
			} else {
				$html_output .= $content;
			}
		}

		if ($this->_numbers) {

			$html_output = '<li>&nbsp;' . str_replace("\n", "</li>\n<li>&nbsp;", $html_output) . '</li>';
			$this->_output = '<ol>' . str_replace(' ', '&nbsp;', $html_output) . '</ol>';
		} else {
			$this->_output = '<pre>' . $html_output . '</pre>';
		}
	}

}

?>