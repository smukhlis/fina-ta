<?php

require_once dirname(__FILE__) . '/../Renderer.php';
require_once dirname(__FILE__) . '/../Renderer/Array.php';

if (!defined('HL_NUMBERS_LI')) {

	define('HL_NUMBERS_LI', 1);

	define('HL_NUMBERS_TABLE', 2);

}

define('HL_NUMBERS_OL', 1);

define('HL_NUMBERS_UL', 3);

class Text_Highlighter_Renderer_Html extends Text_Highlighter_Renderer_Array {

	var $_numbers = 0;

	var $_numbers_start = 0;

	var $_tabsize = 4;

	var $_output = '';

	var $_style_map = array();

	var $_class_map = array(
			'main' => 'hl-main','table' => 'hl-table','gutter' => 'hl-gutter','brackets' => 'hl-brackets',
			'builtin' => 'hl-builtin','code' => 'hl-code','comment' => 'hl-comment','default' => 'hl-default',
			'identifier' => 'hl-identifier','inlinedoc' => 'hl-inlinedoc','inlinetags' => 'hl-inlinetags',
			'mlcomment' => 'hl-mlcomment','number' => 'hl-number','quotes' => 'hl-quotes',
			'reserved' => 'hl-reserved','special' => 'hl-special','string' => 'hl-string','url' => 'hl-url',
			'var' => 'hl-var',
	);

	var $_doclinks = array();

	function reset() {
		$this->_output = '';
		if (isset($this->_options['numbers'])) {
			$this->_numbers = (int) $this->_options['numbers'];
			if ($this->_numbers != HL_NUMBERS_LI && $this->_numbers != HL_NUMBERS_UL
					&& $this->_numbers != HL_NUMBERS_OL && $this->_numbers != HL_NUMBERS_TABLE) {
				$this->_numbers = 0;
			}
		}
		if (isset($this->_options['tabsize'])) {
			$this->_tabsize = $this->_options['tabsize'];
		}
		if (isset($this->_options['numbers_start'])) {
			$this->_numbers_start = intval($this->_options['numbers_start']);
		}
		if (isset($this->_options['doclinks']) && is_array($this->_options['doclinks'])
				&& !empty($this->_options['doclinks']['url'])) {

			$this->_doclinks = $this->_options['doclinks'];
			if (empty($this->_options['doclinks']['elements'])) {
				$this->_doclinks['elements'] = array(
					'reserved','identifier'
				);
			}
		}
		if (isset($this->_options['style_map'])) {
			$this->_style_map = $this->_options['style_map'];
		}
		if (isset($this->_options['class_map'])) {
			$this->_class_map = array_merge($this->_class_map, $this->_options['class_map']);
		}
		$this->_htmlspecialchars = true;

	}

	function _getFullClassName($class) {
		if (!empty($this->_options['use_language'])) {
			$the_class = $this->_language . '-' . $class;
		} else {
			$the_class = $class;
		}
		return $the_class;
	}

	function finalize() {

		parent::finalize();
		$output = parent::getOutput();
		if (empty($output))
			return;

		$html_output = '';

		$numbers_li = false;

		if ($this->_numbers == HL_NUMBERS_LI || $this->_numbers == HL_NUMBERS_UL || $this->_numbers == HL_NUMBERS_OL) {
			$numbers_li = true;
		}

		foreach ($output AS $token) {

			if ($this->_enumerated) {
				$key = false;
				$the_class = $token[0];
				$content = $token[1];
			} else {
				$key = key($token);
				$the_class = $key;
				$content = $token[$key];
			}

			$span = $this->_getStyling($the_class);
			$decorated_output = $this->_decorate($content, $key);
			$html_output .= sprintf($span, $decorated_output);
		}

		if (!empty($this->_numbers) && $numbers_li == true) {

			$this->_output = '<li><pre>&nbsp;' . str_replace("\n", "</pre></li>\n<li><pre>&nbsp;", $html_output)
					. '</pre></li>';

			$start = '';
			if ($this->_numbers == HL_NUMBERS_OL && intval($this->_numbers_start) > 0) {
				$start = ' start="' . $this->_numbers_start . '"';
			}

			$list_tag = 'ol';
			if ($this->_numbers == HL_NUMBERS_UL) {
				$list_tag = 'ul';
			}

			$this->_output = '<' . $list_tag . $start . ' ' . $this->_getStyling('main', false) . '>' . $this->_output
					. '</' . $list_tag . '>';

		} else if ($this->_numbers == HL_NUMBERS_TABLE) {

			$start_number = 0;
			if (intval($this->_numbers_start)) {
				$start_number = $this->_numbers_start - 1;
			}

			$numbers = '';

			$nlines = substr_count($html_output, "\n") + 1;
			for ($i = 1; $i <= $nlines; $i++) {
				$numbers .= ($start_number + $i) . "\n";
			}
			$this->_output = '<table ' . $this->_getStyling('table', false) . ' width="100%"><tr>' . '<td '
					. $this->_getStyling('gutter', false) . ' align="right" valign="top">' . '<pre>' . $numbers
					. '</pre></td><td ' . $this->_getStyling('main', false) . ' valign="top"><pre>' . $html_output
					. '</pre></td></tr></table>';
		}
		if (!$this->_numbers) {
			$this->_output = '<pre>' . $html_output . '</pre>';
		}
		$this->_output = '<div ' . $this->_getStyling('main', false) . '>' . $this->_output . '</div>';
	}

	function _decorate($content, $key = false) {
		if (!empty($this->_doclinks) && !empty($this->_doclinks['url']) && in_array($key, $this->_doclinks['elements'])) {

			$link = '<a href="' . sprintf($this->_doclinks['url'], $content) . '"';
			if (!empty($this->_doclinks['target'])) {
				$link .= ' target="' . $this->_doclinks['target'] . '"';
			}
			$link .= '>';
			$link .= $content;
			$link .= '</a>';

			$content = $link;

		}

		return $content;
	}

	function _getStyling($class, $span_tag = true) {
		$attrib = '';
		if (!empty($this->_style_map) && !empty($this->_style_map[$class])) {
			$attrib = 'style="' . $this->_style_map[$class] . '"';
		}
		if (!empty($this->_class_map) && !empty($this->_class_map[$class])) {
			if ($attrib) {
				$attrib .= ' ';
			}
			$attrib .= 'class="' . $this->_getFullClassName($this->_class_map[$class]) . '"';
		}

		if ($span_tag) {
			$span = '<span ' . $attrib . '>%s</span>';
			return $span;
		} else {
			return $attrib;
		}

	}
}

?>