<?php

require_once dirname(__FILE__) . '/../Renderer.php';

class Text_Highlighter_Renderer_Array extends Text_Highlighter_Renderer {

	var $_tabsize = 4;

	var $_htmlspecialchars = true;

	var $_enumerated = false;

	var $_output = array();

	function preprocess($str) {
		$str = str_replace("\r\n", "\n", $str);
		$str = preg_replace('~^$~m', " ", $str);
		$str = str_replace("\t", str_repeat(' ', $this->_tabsize), $str);
		return rtrim($str);
	}

	function reset() {
		$this->_output = array();
		$this->_lastClass = 'default';
		if (isset($this->_options['tabsize'])) {
			$this->_tabsize = $this->_options['tabsize'];
		}
		if (isset($this->_options['htmlspecialchars'])) {
			$this->_htmlspecialchars = $this->_options['htmlspecialchars'];
		}
		if (isset($this->_options['enumerated'])) {
			$this->_enumerated = $this->_options['enumerated'];
		}
	}

	function acceptToken($class, $content) {

		$theClass = $this->_getFullClassName($class);
		if ($this->_htmlspecialchars) {
			$content = htmlspecialchars($content);
		}
		if ($this->_enumerated) {
			$this->_output[] = array(
				$class,$content
			);
		} else {
			$this->_output[][$class] = $content;
		}
		$this->_lastClass = $class;

	}

	function _getFullClassName($class) {
		if (!empty($this->_options['use_language'])) {
			$theClass = $this->_language . '-' . $class;
		} else {
			$theClass = $class;
		}
		return $theClass;
	}

	function getOutput() {
		return $this->_output;
	}
}

?>