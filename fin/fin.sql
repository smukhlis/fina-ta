-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 09, 2014 at 03:24 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fin`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_telp` varchar(255) DEFAULT NULL,
  `nilai_kompetensi_padagogig_id` int(11) DEFAULT NULL,
  `nilai_kompetensi_pengembangan_guru_id` int(11) DEFAULT NULL,
  `nilai_kepribadian_dan_sosial_guru_id` int(11) DEFAULT NULL,
  `nilai_masa_jabatan_id` int(11) DEFAULT NULL,
  `nilai_status_pandidikan_terakhir_id` int(11) DEFAULT NULL,
  `nilai_prestasi_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nilai_kompetensi_padagogig_id` (`nilai_kompetensi_padagogig_id`),
  KEY `nilai_kompetensi_pengembangan_guru_id` (`nilai_kompetensi_pengembangan_guru_id`),
  KEY `nilai_kepribadian_dan_sosial_guru_id` (`nilai_kepribadian_dan_sosial_guru_id`),
  KEY `nilai_masa_jabatan_id` (`nilai_masa_jabatan_id`),
  KEY `nilai_status_pandidikan_terakhir_id` (`nilai_status_pandidikan_terakhir_id`),
  KEY `nilai_prestasi` (`nilai_prestasi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `nama`, `alamat`, `no_telp`, `nilai_kompetensi_padagogig_id`, `nilai_kompetensi_pengembangan_guru_id`, `nilai_kepribadian_dan_sosial_guru_id`, `nilai_masa_jabatan_id`, `nilai_status_pandidikan_terakhir_id`, `nilai_prestasi_id`) VALUES
(1, 'Inar Garmarini', 'Buduran Sidoarjo', '08175074040', 1, 2, 1, 1, 2, 2),
(2, 'Ir. Diah Kuswardani', '-', '-', 3, 3, 1, 1, 3, 2),
(3, 'Ema Ainul Yatim, A.Ma', 'Lamongan', '085755015822', 2, 1, 1, 1, 2, 2),
(4, 'Erna Hamidha, SPsi', '-', '-', 2, 1, 1, 2, 3, 2),
(5, 'Yuni Setiyowati, ST', '-', '-', 3, 3, 1, 3, 3, 2),
(6, 'Ayu Widayanti, S.Pdi', '-', '-', 2, 3, 1, 3, 3, 2),
(7, 'Rahayu Widosari, A.Md', '-', '-', 3, 3, 1, 3, 2, 1),
(8, 'Enik Kustianingsih', '-', '-', 1, 1, 1, 3, 1, 2),
(9, 'Anis', '-', '-', 2, 3, 2, 3, 4, 1),
(10, 'Nidaur Rohmah', '-', '-', 2, 1, 1, 1, 1, 2),
(11, 'Sri Handayani', '-', '-', 2, 2, 1, 1, 4, 2),
(12, 'Winarti', '-', '-', 2, 2, 2, 3, 4, 2),
(13, 'Rina Purnamasari', '-', '-', 2, 3, 2, 2, 4, 2),
(14, 'Endah Suwati', '-', '-', 2, 3, 1, 2, 1, 2),
(15, 'Retnowati', '-', '-', 2, 3, 1, 3, 4, 1),
(16, 'Anik Zulaikah', '-', '-', 2, 2, 1, 2, 4, 1),
(17, 'A''an Almaidah', '-', '-', 2, 2, 2, 3, 3, 1),
(18, 'Nining Nasifah, S.Pdi', '-', '-', 2, 2, 1, 1, 3, 2),
(19, 'Kasmini', '-', '-', 3, 3, 1, 3, 1, 1),
(20, 'Ernawati', '-', '-', 2, 2, 1, 3, 1, 2),
(21, 'Lulum Mahmudah, A.Md', '-', '-', 2, 2, 1, 3, 2, 1),
(22, 'Rahayu Henny W.,SE', '-', '-', 2, 1, 1, 2, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kepribadian_dan_sosial_guru`
--

CREATE TABLE IF NOT EXISTS `nilai_kepribadian_dan_sosial_guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_padagogig` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `nilai_kepribadian_dan_sosial_guru`
--

INSERT INTO `nilai_kepribadian_dan_sosial_guru` (`id`, `nilai_padagogig`, `kategori`) VALUES
(1, 'Baik', 'Baik'),
(2, 'Kurang Baik', 'Kurang Baik');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kompetensi_padagogig`
--

CREATE TABLE IF NOT EXISTS `nilai_kompetensi_padagogig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_padagogig` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `nilai_kompetensi_padagogig`
--

INSERT INTO `nilai_kompetensi_padagogig` (`id`, `nilai_padagogig`, `kategori`) VALUES
(1, 'Tinggi', 'Tinggi'),
(2, 'Sedang', 'Sedang'),
(3, 'Rendah', 'Rendah');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kompetensi_pengembangan_guru`
--

CREATE TABLE IF NOT EXISTS `nilai_kompetensi_pengembangan_guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_padagogig` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `nilai_kompetensi_pengembangan_guru`
--

INSERT INTO `nilai_kompetensi_pengembangan_guru` (`id`, `nilai_padagogig`, `kategori`) VALUES
(1, 'Baik', 'B'),
(2, 'Cukup', 'C'),
(3, 'Kurang', 'K');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_masa_jabatan`
--

CREATE TABLE IF NOT EXISTS `nilai_masa_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masa_jabatan` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `nilai_masa_jabatan`
--

INSERT INTO `nilai_masa_jabatan` (`id`, `masa_jabatan`, `kategori`) VALUES
(1, '> 2,1 thn', 'Lama'),
(2, '1,1 - 2 thn', 'Sedang'),
(3, '0 - 1 thn', 'Sebentar');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_prestasi`
--

CREATE TABLE IF NOT EXISTS `nilai_prestasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_prestasi` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `nilai_prestasi`
--

INSERT INTO `nilai_prestasi` (`id`, `nilai_prestasi`, `kategori`) VALUES
(1, 'Belum', 'Belum'),
(2, 'Berprestasi', 'Berprestasi');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE IF NOT EXISTS `prestasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prestasi` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_pendidikan_terakhir`
--

CREATE TABLE IF NOT EXISTS `status_pendidikan_terakhir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_pendidikan_terakhir` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `status_pendidikan_terakhir`
--

INSERT INTO `status_pendidikan_terakhir` (`id`, `status_pendidikan_terakhir`, `kategori`) VALUES
(1, 'SMA', 'SMA'),
(2, 'D2', 'D2'),
(3, 'S1', 'S1'),
(4, 'Lain-lain', 'Lain-lain');

-- --------------------------------------------------------

--
-- Table structure for table `usr`
--

CREATE TABLE IF NOT EXISTS `usr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `privilege` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `privilege` (`privilege`),
  KEY `type` (`type`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usr`
--

INSERT INTO `usr` (`id`, `username`, `password`, `email`, `privilege`, `target`, `type`, `status`) VALUES
(1, 'admin', 'e0478b5f2ed5d403dd947875244a22b1', 'admin@gmail.com', 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usr_privilege`
--

CREATE TABLE IF NOT EXISTS `usr_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `usr_privilege`
--

INSERT INTO `usr_privilege` (`id`, `name`, `note`) VALUES
(1, 'Administrator', 'Super User'),
(2, 'Writer', 'User yang dapat mengisi content'),
(3, 'Reader', 'User yang dapat melihat data administrasi web'),
(4, 'Member', 'Hak Akses Member setelah Login');

-- --------------------------------------------------------

--
-- Table structure for table `usr_status`
--

CREATE TABLE IF NOT EXISTS `usr_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `usr_status`
--

INSERT INTO `usr_status` (`id`, `name`, `note`) VALUES
(1, 'Aktif', 'Account yang di izinkan login'),
(2, 'Suspend', 'Account yang tidak di izinkan login');

-- --------------------------------------------------------

--
-- Table structure for table `usr_type`
--

CREATE TABLE IF NOT EXISTS `usr_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `usr_type`
--

INSERT INTO `usr_type` (`id`, `name`, `note`) VALUES
(1, 'Admin', 'Pengelola Website'),
(2, 'Member', 'Pengguna ataupun pelanggan website');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`nilai_kompetensi_padagogig_id`) REFERENCES `nilai_kompetensi_padagogig` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_3` FOREIGN KEY (`nilai_kepribadian_dan_sosial_guru_id`) REFERENCES `nilai_kepribadian_dan_sosial_guru` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_4` FOREIGN KEY (`nilai_masa_jabatan_id`) REFERENCES `nilai_masa_jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_5` FOREIGN KEY (`nilai_status_pandidikan_terakhir_id`) REFERENCES `status_pendidikan_terakhir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_6` FOREIGN KEY (`nilai_prestasi_id`) REFERENCES `nilai_prestasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guru_ibfk_7` FOREIGN KEY (`nilai_kompetensi_pengembangan_guru_id`) REFERENCES `nilai_kompetensi_pengembangan_guru` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usr`
--
ALTER TABLE `usr`
  ADD CONSTRAINT `usr_ibfk_1` FOREIGN KEY (`privilege`) REFERENCES `usr_privilege` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usr_ibfk_2` FOREIGN KEY (`type`) REFERENCES `usr_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usr_ibfk_3` FOREIGN KEY (`status`) REFERENCES `usr_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
