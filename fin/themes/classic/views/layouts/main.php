<?php
$cs = &Yii::app()->clientScript;
$themePath = Yii::app()->theme->baseUrl;

/**
 * StyleSHeets
 */
$cs->registerCssFile($themePath . '/bootstrap/css/bootstrap.css');
$cs->registerCssFile($themePath . '/css/jquery.jOrgChart.css');
//$cs->registerCssFile($themePath . '/css/custom.css');
$cs->registerCssFile($themePath . '/css/prettify.css');
//$cs->registerCssFile($themePath . '/bootstrap/css/bootstrap-theme.css');

/**
 * JavaScripts
 */
$cs->registerCoreScript('jquery', CClientScript::POS_END);
$cs->registerCoreScript('jquery.ui', CClientScript::POS_END);
$cs->registerScriptFile($themePath . '/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);
$cs->registerScriptFile($themePath . '/js/prettify.js', CClientScript::POS_END);
$cs->registerScriptFile($themePath . '/js/prettify.js', CClientScript::POS_END);
$cs->registerScriptFile($themePath . '/js/jquery.jOrgChart.js', CClientScript::POS_END);
$cs
		->registerScript('tooltip',
				"$('[data-toggle=\"tooltip\"]').tooltip();$('[data-toggle=\"popover\"]').tooltip()",
				CClientScript::POS_READY);


?>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="<?php
echo Yii::app()->theme->baseUrl . '/assets/js/html5shiv.js';
				 ?>"></script>
    <script src="<?php
				 echo Yii::app()->theme->baseUrl . '/assets/js/respond.min.js';
				 ?>"></script>
<![endif]-->

</head>
  <body>
<div id="wrap">
    <!-- Fixed navbar -->
    <?php $this->renderPartial('//layouts/navbar') ?>
    <!-- Begin page content -->
    <div class="container">
        <?php echo $content ?>
    </div>
</div>
  </body>
</html>
