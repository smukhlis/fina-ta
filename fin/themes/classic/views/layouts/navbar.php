 <?php
$this
		->widget('bootstrap.widgets.BsNavbar',
				array(
						'collapse' => true,'brandLabel' => BSHtml::icon(BSHtml::GLYPHICON_HOME),
						'brandUrl' => Yii::app()->homeUrl,
						'items' => array(
								array(
										'class' => 'bootstrap.widgets.BsNav','type' => 'navbar',
										'activateParents' => true,
										'items' => array(
												array(
														'label' => 'Home',
														'url' => array(
															'/site/index'
														),
														//'items' => array()
												)
										)
								),
								array(
										'class' => 'bootstrap.widgets.BsNav','type' => 'navbar',
										'activateParents' => true,
										'items' => array(
												array(
														'label' => 'Master',
														'url' => array(
															'/master/index'
														),
														'items' => array(
																array(
																		'label' => 'Nilai Kompetensi Pedagogik',
																		'url' => array(
																			'/nilai-kompetensi-padagogig/admin'
																		)
																),
																array(
																		'label' => 'Nilai Kompetensi Pengembangan Guru',
																		'url' => array(
																			'/nilai-kompetensi-pengembangan-guru/admin'
																		)
																),
																array(
																		'label' => 'Nilai Kepribadian Dan Sosial Guru',
																		'url' => array(
																			'/nilai-kepribadian-dan-sosial-guru/admin',
																		)
																),																																
																array(
																		'label' => 'Nilai Masa Jabatan',
																		'url' => array(
																			'/nilai-masa-jabatan/admin'
																		)
																),
																array(
																		'label' => 'Status Pendidikan Terakhir',
																		'url' => array(
																			'/status-pendidikan-terakhir/admin'
																		)
																),
																array(
																		'label' => 'Nilai Prestasi',
																		'url' => array(
																			'/nilai-prestasi/admin'
																		)
																),BSHtml::menuDivider(),
																array(
																		'label' => 'Logout (' . Yii::app()->user->name
																				. ')',
																		'url' => array(
																			'/site/logout'
																		),'visible' => !Yii::app()->user->isGuest
																),
																
														)
												),
												array(
														'label' => 'Guru',
														'url' => array(
																'/guru/admin'
														),
												),
												array(
														'label' => 'Pohon Keputusan',
														'url' => array(
																'/site/pohon'
														),
												),
												array(
														'label' => 'Klasifikasi Guru',
														'url' => array(
																'/site/klasifikasi-guru'
														),
												),
										)
								),
								array(
										'class' => 'bootstrap.widgets.BsNav','type' => 'navbar',
										'activateParents' => true,
										'items' => array(
												array(
														'label' => 'Login',
														'url' => array(
															'/site/login'
														),'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT,
														'visible' => Yii::app()->user->isGuest
												),
												array(
														'label' => 'Logout (' . Yii::app()->user->name . ')',
														'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT,
														'url' => array(
															'/site/logout'
														),'visible' => !Yii::app()->user->isGuest
												)
										),
										'htmlOptions' => array(
											'pull' => BSHtml::NAVBAR_NAV_PULL_RIGHT
										)
								)
						)
				));
 ?>