<!doctype html>
<html lang="en-US"><head>
<meta charset="UTF-8">
<title><?php echo $this->page['title']; ?></title>
<meta name="description" content="<?php echo $this->page['description']; ?>">
<meta name="keywords" content="<?php echo $this->page['keywords']; ?>">
<meta name="language" content="id" />
<meta name="generator" content="WordPress 2.9.1"/>
<meta name="author" content="herbalinshop"/>
<meta name="robots" content="index, follow"/>
<meta content="1 days" name="revisit-after"/>
<link rel="icon" type="image/png" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.png">

<!--=================================== Mobile Specific Meta  ===================================-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

<link rel="stylesheet"  href="<?php echo Yii::app()->theme->baseUrl; ?>/css/b.css" type="text/css" media="all">
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/comment-reply.min.js"></script>
<meta name="generator" content="WordPress 3.5.1">

<body>
<?php echo $content; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42392574-1', 'herbalinshop.com');
  ga('send', 'pageview');

</script>
</body></html>