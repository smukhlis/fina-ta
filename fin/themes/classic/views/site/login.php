<style type="text/css">
<!--
.style1 {
	color: #0033FF;
	font-family: "Monotype Corsiva";
	font-size: 36px;
}
body {
	background-color: #FFFFFF;
}
.style3 {font-size: 36px}
-->
</style>


<div class="container">
<?php

$form = $this
		->beginWidget('CActiveForm',
				array(
						'id' => 'login-form','enableAjaxValidation' => false,
						'clientOptions' => array(
							'validateOnSubmit' => true
						),'htmlOptions' => array(
							'role' => 'form'
						)
				));
?>
	<div class="form-signin">
		<strong><span class="form-signin-heading"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/lasiyam.jpg" width="143" height="159"></span>	<span class="form-signin-heading style3">KB - TK - TPA LASIYAM</span></strong>
		<form action="" method="post" name="form1">
		  <p><strong>Jl. Residen Sudirman No. 31, Surabaya</strong></p>
		  <p><strong>Telp . (031) 5030955</strong></p>
		  <p><strong>E-mail : lasi_yam@yahoo.com </strong></p>
		</form>
		<h2 class="form-signin-heading">
		  <marquee>
		  <span class="style1">Selamat Datang di Sistem Penentuan Prestasi Guru KB-TK-TPA Lasiyam dengan menggunakan Metode ID3 </span>
	      </marquee>
        </h2>
		<h2 class="form-signin-heading">Please Sign In</h2>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'username'); ?>
	        <section  class="controls">
			<?php echo $form->textField($model, 'username', array(
			'class' => 'form-control'
		)); ?>
			<?php echo $form->error($model, 'username', array(
			'class' => 'help-inline'
		)); ?>
          </section>
		</div>
	
		<div class="form-group">
			<?php echo $form->labelEx($model, 'password'); ?>
	                <section  class="controls">
			<?php echo $form->passwordField($model, 'password', array(
			'class' => 'form-control'
		)); ?>
			<?php echo $form->error($model, 'password', array(
			'class' => 'help-inline'
		)); ?>
	                </section>
		</div>
		
		<div class="checkbox">
		    <label>
		      <?php echo $form->checkBox($model, 'rememberMe'); ?> Remember me
		    </label>
		    <?php echo $form->error($model, 'rememberMe'); ?>
		</div>
	
		<button type="submit" class="btn btn-default">Login</button>
  </div>
	
<?php $this->endWidget(); ?>

</div>
<!-- /container -->