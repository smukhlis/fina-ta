<?php
/* @var $this GuruController */
/* @var $model Guru */
/* @var $form BSActiveForm */
?>

<div class="form">

    <?php $form = $this
		->beginWidget('bootstrap.widgets.BsActiveForm',
				array(
					'id' => 'guru-form',
					// Please note: When you enable ajax validation, make sure the corresponding
					// controller action is handling ajax validation correctly.
					// There is a call to performAjaxValidation() commented in generated controller code.
					// See class documentation of CActiveForm for details on this.
					'enableAjaxValidation' => false,'layout' => BSHtml::FORM_LAYOUT_VERTICAL,
				));
	?>

    <p class="help-block">&nbsp;</p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldControlGroup($model, 'nama', array(
			'maxlength' => 255
		)); ?>

            <?php echo $form->textFieldControlGroup($model, 'alamat', array(
			'maxlength' => 255
		)); ?>

            <?php echo $form->textFieldControlGroup($model, 'no_telp', array(
			'maxlength' => 255
		)); ?>
            
            <?php
echo $form
		->dropDownListControlGroup($model, 'nilai_kompetensi_padagogig_id',
				CHtml::listData(NilaiKompetensiPadagogig::model()->findAll(), 'id', 'kategori'),
				array(
					'empty' => 'Pilih Nilai Kompetensi Padagogig'
				));
			?>
			
			<?php
echo $form
		->dropDownListControlGroup($model, 'nilai_kompetensi_pengembangan_guru_id',
				CHtml::listData(NilaiKompetensiPengembanganGuru::model()->findAll(), 'id', 'kategori'),
				array(
					'empty' => 'Pilih Nilai Kompetensi Pengembangan Guru'
				));
			?>
			
			<?php
echo $form
		->dropDownListControlGroup($model, 'nilai_kepribadian_dan_sosial_guru_id',
				CHtml::listData(NilaiKepribadianDanSosialGuru::model()->findAll(), 'id', 'kategori'),
				array(
					'empty' => 'Pilih Nilai Kepribadian dan Sosial Guru'
				));
			?>
			
			<?php
echo $form
		->dropDownListControlGroup($model, 'nilai_masa_jabatan_id',
				CHtml::listData(NilaiMasaJabatan::model()->findAll(), 'id', 'kategori'),
				array(
					'empty' => 'Pilih Nilai Masa Jabatan'
				));
			?>
			
			<?php
echo $form
		->dropDownListControlGroup($model, 'nilai_status_pandidikan_terakhir_id',
				CHtml::listData(StatusPendidikanTerakhir::model()->findAll(), 'id', 'kategori'),
				array(
					'empty' => 'Pilih Nilai Pendidikan Terakhir'
				));
			?>
			
			
			<?php

			?>

            <?php // echo $form->textFieldControlGroup($model,'nilai_kompetensi_padagogig_id'); ?>

            <?php // echo $form->textFieldControlGroup($model,'nilai_kompetensi_pengembangan_guru_id'); ?>

            <?php // echo $form->textFieldControlGroup($model,'nilai_kepribadian_dan_sosial_guru_id'); ?>

            <?php // echo $form->textFieldControlGroup($model,'nilai_masa_jabatan_id'); ?>

            <?php // echo $form->textFieldControlGroup($model,'nilai_status_pandidikan_terakhir_id'); ?>

            <?php echo BSHtml::submitButton('Submit', array(
	'color' => BSHtml::BUTTON_COLOR_PRIMARY
)); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->