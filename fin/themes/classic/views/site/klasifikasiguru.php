<?php

function array_sort($array, $on, $order = 1) {
	$new_array = array();
	$sortable_array = array();

	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch ($order) {
			case 1:
				asort($sortable_array);
				break;
			case 2:
				arsort($sortable_array);
				break;
		}

		foreach ($sortable_array as $k => $v) {
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

// mengambil data guru
$gurus = Guru::model()->findAll();
$nilaiPrestasis = NilaiPrestasi::model()->findAll();

$entropiAwal = $this->hitungEntropi($gurus, $nilaiPrestasis, 'Entropi Awal');

$data_kompetensi_array = array();
$gsa_array = array();

// perhitungan kompetensi padagogig
$nilaiKompetensiPadaggigs = NilaiKompetensiPadagogig::model()->findAll();
$data_kompetensi_array[] = NilaiKompetensiPadagogig::model()->findAll();
;

// perhitungan kompetensi pengembangan guru
$data_kompetensi_array[] = NilaiKompetensiPengembanganGuru::model()->findAll();

// perhitungan kepribadian dan sosial
$data_kompetensi_array[] = NilaiKepribadianDanSosialGuru::model()->findAll();

// perhitungan masa jabatan
$data_kompetensi_array[] = NilaiMasaJabatan::model()->findAll();

// perhitungan status pendididkan terakhir
$data_kompetensi_array[] = StatusPendidikanTerakhir::model()->findAll();

foreach ($data_kompetensi_array as $k => $v) {
	$gsa = $this->hitungGsa($gurus, $nilaiPrestasis, $v, $entropiAwal, $v[0]->tableName(), $k);
	// echo 'GSA '.$v[0]->tableName().' = '.$gsa;
	// echo '<br />';
	$gsa_array[$k]['kompetensi'] = $v;
	$gsa_array[$k]['gsa'] = $gsa;
}

// echo '<br />';
// echo 'step 2';
// echo '<br />';

$datas = array();

$gsa_array_sort = array_sort($gsa_array, 'gsa', 2);
$iterator = 0;
foreach ($gsa_array_sort as $k => $value) {
	if ($iterator == 0) {
		// $k = 3
		// echo '<br />' . $k . '--';
		// echo $value['kompetensi'][0]->tableName();
		// echo '==========================================';
		// echo '<br />';
		
		
		$guru_tmp = array();
		foreach ($gurus as $k_g => $v_g) {
			if ($value['kompetensi'][0] instanceof NilaiMasaJabatan) {
				$guru_tmp[$v_g->nilai_masa_jabatan_id][] = $v_g;
			}else if ($value['kompetensi'][0] instanceof NilaiKepribadianDanSosialGuru) {
				$guru_tmp[$v_g->nilai_kepribadian_dan_sosial_guru_id][] = $v_g;
			}else if ($value['kompetensi'][0] instanceof StatusPendidikanTerakhir) {
				$guru_tmp[$v_g->nilai_status_pandidikan_terakhir_id][] = $v_g;
			}else if ($value['kompetensi'][0] instanceof NilaiKompetensiPadagogig) {
				$guru_tmp[$v_g->nilai_kompetensi_padagogig_id][] = $v_g;
			}else if ($value['kompetensi'][0] instanceof NilaiKompetensiPengembanganGuru) {
				$guru_tmp[$v_g->nilai_kompetensi_pengembangan_guru_id][] = $v_g;
			}
		}
		foreach ($guru_tmp as $k_gpg=>$guru_per_gol22) {
			
			// echo '<pre>';
			// print_r($value['kompetensi'][0]->findByPk($k_gpg)->kategori);
			// echo '</pre>';
			
			
			
			$entropiAwal_22 = $this->hitungEntropi($guru_per_gol22, $nilaiPrestasis, 'Entropi Gol');
			// echo 'entropi x: ' . $entropiAwal_22;
			// echo '<br />';
			
			if ($entropiAwal_22==0) {
				$tmp_22=1;
				foreach ($guru_per_gol22 as $g22) {
					$tmp_22= $g22->nilai_prestasi_id;
				}
				$datas[$value['kompetensi'][0]->tableName()][$value['kompetensi'][0]->findByPk($k_gpg)->kategori]=$tmp_22;
			}
			
			if ($entropiAwal_22!=0) {
				$gsa_array_22 = array();
				
				foreach ($data_kompetensi_array as $k_22 => $v_22) {
					if ($k_22 != $k) {
						$gsa = $this
						->hitungGsa($guru_per_gol22, $nilaiPrestasis, $v_22, $entropiAwal_22, $v_22[0]->tableName(),
								$k_22);
						// echo 'GSA' . $v_22[0]->tableName() . ' = ' . $gsa;
						// echo '<br />';
						// echo '==========================================';
						// echo '<br />';
						$gsa_array_22[$k_22]['kompetensi'] = $v_22;
						$gsa_array_22[$k_22]['gsa'] = $gsa;
					}
					
				
				}
				// echo '<br />';
				// echo 'step 3';
				// echo '<br />';
				
				$gsa_array_sort333 = array_sort($gsa_array_22, 'gsa', 2);
				$iterator333 = 0;
				foreach ($gsa_array_sort333 as $k333 => $value333) {
					if ($iterator333 == 0) {
						// $k = 3
						// echo '<br />' . $k333 . '--';
						// echo $value333['kompetensi'][0]->tableName();
						// echo '==========================================';
						// echo '<br />';
						
						
						$guru_tmp333 = array();
						foreach ($guru_per_gol22 as $k_g333 => $v_g333) {
							if ($value333['kompetensi'][0] instanceof NilaiMasaJabatan) {
								$guru_tmp333[$v_g333->nilai_masa_jabatan_id][] = $v_g333;
							}else if ($value333['kompetensi'][0] instanceof NilaiKepribadianDanSosialGuru) {
								$guru_tmp333[$v_g333->nilai_kepribadian_dan_sosial_guru_id][] = $v_g333;
							}else if ($value333['kompetensi'][0] instanceof StatusPendidikanTerakhir) {
								$guru_tmp333[$v_g333->nilai_status_pandidikan_terakhir_id][] = $v_g333;
							}else if ($value333['kompetensi'][0] instanceof NilaiKompetensiPadagogig) {
								$guru_tmp333[$v_g333->nilai_kompetensi_padagogig_id][] = $v_g333;
							}else if ($value333['kompetensi'][0] instanceof NilaiKompetensiPengembanganGuru) {
								$guru_tmp333[$v_g333->nilai_kompetensi_pengembangan_guru_id][] = $v_g333;
							}
						}
						
						foreach ($guru_tmp333 as $k_gpg333=>$guru_per_gol333) {
							// echo 'jumlah'.count($guru_per_gol333);
							// echo '<pre>';
							// print_r($value333['kompetensi'][0]->findByPk($k_gpg333)->kategori);
							// echo '</pre>';
				
				
							$entropiAwal_333 = $this->hitungEntropi($guru_per_gol333, $nilaiPrestasis, 'Entropi Gol');
							// echo 'entropi x: ' . $entropiAwal_333;
							// echo '<br />';
							
							if ($entropiAwal_333==0) {
								$tmp_22=1;
								foreach ($guru_per_gol333 as $g333) {
									$tmp_22= $g333->nilai_prestasi_id;
									// echo '<br />';
								}
								$datas[$value['kompetensi'][0]->tableName()][$value['kompetensi'][0]->findByPk($k_gpg)->kategori][$value333['kompetensi'][0]->tableName()][$value333['kompetensi'][0]->findByPk($k_gpg333)->kategori]=$tmp_22;
							}
				
							if ($entropiAwal_333!=0) {
								$gsa_array_333 = array();
				
								foreach ($data_kompetensi_array as $k_333 => $v_333) {
									if ($k_333 != $k333 && $k_333 != $k ) {
										$gsa = $this
										->hitungGsa($guru_per_gol333, $nilaiPrestasis, $v_333, $entropiAwal_333, $v_333[0]->tableName(),
												$k_333);
										// echo 'GSA' . $v_333[0]->tableName() . ' = ' . $gsa;
										// echo '<br />';
										// echo '==========================================';
										// echo '<br />';
										$gsa_array_333[$k_333]['kompetensi'] = $v_333;
										$gsa_array_333[$k_333]['gsa'] = $gsa;
									}
				
				
								}
								
									// step 4
								// echo '<br />';
								// echo 'step 4';
								// echo '<br />';
									
								$gsa_array_sort4444 = array_sort($gsa_array_333, 'gsa', 2);
								$iterator4444 = 0;
								foreach ($gsa_array_sort4444 as $k4444 => $value4444) {
									if ($iterator4444 == 0) {
										// $k = 3
										// echo '<br />' . $k4444 . '--';
										// echo $value4444['kompetensi'][0]->tableName();
										// echo '==========================================';
										// echo '<br />';
										
										
										$guru_tmp4444 = array();
										foreach ($guru_per_gol333 as $k_g4444 => $v_g4444) {
											if ($value4444['kompetensi'][0] instanceof NilaiMasaJabatan) {
												$guru_tmp4444[$v_g4444->nilai_masa_jabatan_id][] = $v_g4444;
											}else if ($value4444['kompetensi'][0] instanceof NilaiKepribadianDanSosialGuru) {
												$guru_tmp4444[$v_g4444->nilai_kepribadian_dan_sosial_guru_id][] = $v_g4444;
											}else if ($value4444['kompetensi'][0] instanceof StatusPendidikanTerakhir) {
												$guru_tmp4444[$v_g4444->nilai_status_pandidikan_terakhir_id][] = $v_g4444;
											}else if ($value4444['kompetensi'][0] instanceof NilaiKompetensiPadagogig) {
												$guru_tmp4444[$v_g4444->nilai_kompetensi_padagogig_id][] = $v_g4444;
											}else if ($value4444['kompetensi'][0] instanceof NilaiKompetensiPengembanganGuru) {
												$guru_tmp4444[$v_g4444->nilai_kompetensi_pengembangan_guru_id][] = $v_g4444;
											}
										}
										foreach ($guru_tmp4444 as $k_gpg4444=>$guru_per_gol4444) {
								
											// echo '<pre>';
											// print_r($value4444['kompetensi'][0]->findByPk($k_gpg4444)->kategori);
											// echo '</pre>';
								
								
											$entropiAwal_4444 = $this->hitungEntropi($guru_per_gol4444, $nilaiPrestasis, 'Entropi Gol');
											// echo 'entropi x: ' . $entropiAwal_4444;
											// echo '<br />';
											
											if ($entropiAwal_4444==0) {
												$tmp_22=1;
												foreach ($guru_per_gol4444 as $g4444) {
													$tmp_22= $g4444->nilai_prestasi_id;
													// echo '<br />';
												}
												$datas[$value['kompetensi'][0]->tableName()][$value['kompetensi'][0]->findByPk($k_gpg)->kategori][$value333['kompetensi'][0]->tableName()]
												[$value333['kompetensi'][0]->findByPk($k_gpg333)->kategori][$value4444['kompetensi'][0]->tableName()][$value4444['kompetensi'][0]->findByPk($k_gpg4444)->kategori]=$tmp_22;
											}
								
											if ($entropiAwal_4444!=0) {
												$gsa_array_4444 = array();
								
												foreach ($data_kompetensi_array as $k_4444 => $v_4444) {
													if ($k_4444 != $k4444 && $k_4444 != $k && $k_4444 != $k333 ) {
														$gsa = $this
														->hitungGsa($guru_per_gol4444, $nilaiPrestasis, $v_4444, $entropiAwal_4444, $v_4444[0]->tableName(),
																$k_4444);
														// echo 'GSA' . $v_4444[0]->tableName() . ' = ' . $gsa;
														// echo '<br />';
														// echo '==========================================';
														// echo '<br />';
														$gsa_array_4444[$k_4444]['kompetensi'] = $v_4444;
														$gsa_array_4444[$k_4444]['gsa'] = $gsa;
													}
								
												}
								
												//step 5
												// echo '<br />';
												// echo 'step 5';
												// echo '<br />';
												$gsa_array_sort55555 = array_sort($gsa_array_4444, 'gsa', 2);
												$iterator55555 = 0;
												foreach ($gsa_array_sort55555 as $k55555 => $value55555) {
													if ($iterator55555 == 0) {
														// $k = 3
														// echo '<br />' . $k55555 . '--';
														// echo $value55555['kompetensi'][0]->tableName();
														// echo '==========================================';
														// echo '<br />';
														$guru_tmp55555 = array();
														foreach ($guru_per_gol333 as $k_g55555 => $v_g55555) {
															if ($value55555['kompetensi'][0] instanceof NilaiMasaJabatan) {
																$guru_tmp55555[$v_g55555->nilai_masa_jabatan_id][] = $v_g55555;
															}else if ($value55555['kompetensi'][0] instanceof NilaiKepribadianDanSosialGuru) {
																$guru_tmp55555[$v_g55555->nilai_kepribadian_dan_sosial_guru_id][] = $v_g55555;
															}else if ($value55555['kompetensi'][0] instanceof StatusPendidikanTerakhir) {
																$guru_tmp55555[$v_g55555->nilai_status_pandidikan_terakhir_id][] = $v_g55555;
															}else if ($value55555['kompetensi'][0] instanceof NilaiKompetensiPadagogig) {
																$guru_tmp55555[$v_g55555->nilai_kompetensi_padagogig_id][] = $v_g55555;
															}else if ($value55555['kompetensi'][0] instanceof NilaiKompetensiPengembanganGuru) {
																$guru_tmp55555[$v_g55555->nilai_kompetensi_pengembangan_guru_id][] = $v_g55555;
															}
														}
														foreach ($guru_tmp55555 as $k_gpg55555=>$guru_per_gol55555) {
												
															// echo '<pre>';
															// print_r($value55555['kompetensi'][0]->findByPk($k_gpg55555)->kategori);
															// echo '</pre>';
												
												
															$entropiAwal_55555 = $this->hitungEntropi($guru_per_gol55555, $nilaiPrestasis, 'Entropi Gol');
															// echo 'entropi x: ' . $entropiAwal_55555;
															// echo '<br />';
															
															if ($entropiAwal_55555==0) {
																foreach ($guru_per_gol55555 as $g55555) {
																	// echo $g55555->nilai_prestasi_id;
																	// echo '<br />';
																}
															}
												
															if ($entropiAwal_55555!=0) {
																$gsa_array_55555 = array();
												
																foreach ($data_kompetensi_array as $k_55555 => $v_55555) {
																	if ($k_55555 != $k55555 && $k_55555 != $k4444 && $k_55555 != $k333 &&  $k_55555 != $k) {
																		$gsa = $this
																		->hitungGsa($guru_per_gol55555, $nilaiPrestasis, $v_55555, $entropiAwal_55555, $v_55555[0]->tableName(),
																				$k_55555);
																		// echo 'GSA' . $v_55555[0]->tableName() . ' = ' . $gsa;
																		// echo '<br />';
																		// echo '==========================================';
																		// echo '<br />';
																		$gsa_array_55555[$k_55555]['kompetensi'] = $v_55555;
																		$gsa_array_55555[$k_55555]['gsa'] = $gsa;
																	}
												
																}
												
																//step 5
															}
												
												
												
														}
												
													}
													$iterator55555++;
												
												}
											}
								
								
								
										}
								
									}
									$iterator4444++;
								
								}
							}
				
				
				
						}
				
					}
					$iterator333++;
				
				}
			}



		}

	}
	$iterator++;

}

// echo '<pre>';
// print_r($datas);
// echo '</pre>';
function ec($v){
	$v = str_ireplace('nilai', '', $v);
	$v = str_ireplace('_', ' ', $v);
	if ($v==2) {
		return 'Ber prestasi';
	}else if ($v==1) {
		return 'Belum ber prestasi';
	}else if ($v==='nilai_kompetensi_pengembangan_guru') {
		return 'Pengembangan Guru';
	}else if ($v==='status_pendidikan_terakhir') {
		return 'Pendidikan Terakhir';
	}else if ($v==='nilai_kepribadian_dan_sosial_guru') {
		return 'Kepribadian dan sosial guru';
	}else if ($v==='nilai_kompetensi_padagogig') {
		return 'Kompetensi padegogig';
	}else if ($v==='Kurang Baik') {
		return 'Kurang Baik';
	}else if ($v==='Baik') {
		return 'Baik';
	}else if ($v==='Tinggi') {
		return 'Tinggi';
	}else if ($v==='Rendah') {
		return 'Rendah';
	}else if ($v==='Sedang') {
		return 'Sedang';
	}else if ($v==='Lain-lain') {
		return 'Lain lain';
	}else if ($v==='K') {
		return 'Kurang';
	}else if ($v==='C') {
		return 'Cukup';
	}else if ($v==='B') {
		return 'Baik';
	}
	return $v;
}
function arrU($arr){
	$out = '';
	foreach ($arr as $k=>$v) {
		if (is_array($v)) {
			$out.= '<li>'.ec($k).'<ul>'.arrU($v).'</ul></li>';
		}else{
			$out.= '<li>'.ec($k).'<ul>'.'<li>'.ec($v).'</li>'.'</ul></li>';
		}
	}
	return  $out;
}



function c($v, $g){

    //$g = new Guru();
    // nilai_masa_jabatan
    if ($v == "nilai_masa_jabatan") {
        $id = $g->nilai_masa_jabatan_id;
        $nilai = NilaiMasaJabatan::model()->findByPk($id);
        return $nilai->kategori;
    }else if ($v == "nilai_kompetensi_pengembangan_guru") {
        $id = $g->nilai_kompetensi_pengembangan_guru_id;
        $nilai = NilaiKompetensiPengembanganGuru::model()->findByPk($id);
        return $nilai->kategori;

    }else if ($v == "status_pendidikan_terakhir") {
        $id = $g->nilai_status_pandidikan_terakhir_id;
        $nilai = StatusPendidikanTerakhir::model()->findByPk($id);
        return $nilai->kategori;
    }else if ($v == "nilai_kepribadian_dan_sosial_guru") {
        $id = $g->nilai_kepribadian_dan_sosial_guru_id;
        $nilai = NilaiKepribadianDanSosialGuru::model()->findByPk($id);
        return $nilai->kategori;
    }else if ($v == "nilai_kompetensi_padagogig") {
        $id = $g->nilai_kompetensi_padagogig_id;
        $nilai = NilaiKompetensiPadagogig::model()->findByPk($id);
        return $nilai->kategori;
    }

    return false;
}
function cU($arr, $g){

    foreach ($arr as $k=>$v) {
        if (is_array($v)) {
            $kategori = c($k, $g);

            if ($kategori !== false) {

                if (isset($v[$kategori])) {

                    $hasil = $v[$kategori];
                    if (is_array($hasil)) {
                        return cU($hasil, $g);
                    }else{
                        return $hasil;
                    }
                }

            }

        }

    }
    return false;
}
// hapus
/*
 echo "<h1>Pohon ED3</h1>";

 echo '<ul id="org" style="display:none">';
 echo arrU($datas);

 echo '</ul>';

 echo '<div id="chart" class="orgChart"></div>';

$cs = &Yii::app()->clientScript;
$cs
->registerScript('tooltip2',
		"jQuery(document).ready(function() {
        $(\"#org\").jOrgChart({
            chartElement : '#chart',
            dragAndDrop  : false
        });
    });
		
       jQuery(document).ready(function() {
            
         
            $(\"#show-list\").click(function(e){
                e.preventDefault();
                
                $('#list-html').toggle('fast', function(){
                    if($(this).is(':visible')){
                        $('#show-list').text('Hide underlying list.');
                        $(\".topbar\").fadeTo('fast',0.9);
                    }else{
                        $('#show-list').text('Show underlying list.');
                        $(\".topbar\").fadeTo('fast',1);                  
                    }
                });
            });
            
            $('#list-html').text($('#org').html());
            
            $(\"#org\").bind(\"DOMSubtreeModified\", function() {
                $('#list-html').text('');
                
                $('#list-html').text($('#org').html());
                
                prettyPrint();                
            });
        });		
		");
		
		*/
// hapus
//// echo $gsa_array_sort[0]['gsa'];

//$gsa = $this->hitungGsa($gurus, $nilaiPrestasis, $nilaiKompetensiPadaggigs,  $entropiAwal, 'Nilai Kompetensi Padagogig');
// $gsa = $this->hitungGsa($gurus, $nilaiPrestasis, $nilaiKompetensiPadaggigs, $entropiAwal, 'Nilai Kompetensi Padagogig');
// // echo $gsa;



$model = new Guru();
if (isset($_POST['Guru'])) {
    $model->attributes = $_POST['Guru'];
    $tmp =cU($datas, $model);
    $s = "";
    if ($tmp != false) {
        if ($tmp == 1) {
            $s = "Belum ber prestasi";
        }else{
            $s = "Berprestasi";
        }
    }


?>

    <div class="alert alert-info alert-dismissable" style="margin-top: 100px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4>Hasil Perhitungan</h4>
        <b><?php echo $model->nama;?></b>
        <br/>
        Status = <?php echo $s;?>
    </div>
<?php
}

?>
<h1>Proses Guru</h1>
<?php $this->renderPartial('_form', array(
			'model' => $model
		)); ?>