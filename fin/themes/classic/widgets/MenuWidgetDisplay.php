<?php
class MenuWidgetDisplay extends CWidget {
	public $dataMenuParent;
	public $menuid = 1;

	/* 
	 * @see CWidget::init()
	 */public function init() {

	}

	/* 
	 * @see CWidget::run()
	 */public function run() {
		if ($this->dataMenuParent != null) {
			$this->renderParent();
		}
	}

	public function renderParent() {
		foreach ($this->dataMenuParent as $v) {
			$selected = '';
			if ($this->menuid == $v->id) {
				$selected = ' class="current-menu-item"';
			}
			echo '<li' . $selected . '><a href="'
					. Yii::app()->controller->createUrl('site/page', array(
								'id' => $v->link
							)) . '">' . $v->name . '<span class="menu">' . $v->note . '</span></a>';
			if (count($v->childrens) > 0) {
				$this->renderChildrens($v->childrens);
			}
			echo '</li>';

			// 			echo '<li>';
			// 			echo '<a href="#" class="sf-with-ul">'.$v->menu_title.'<span class="description">'.$v->menu_description.'</span><span class="sf-sub-indicator"> &#187;</span></a>';
			// 			if (count($v->childrens)>0) {
			// 				$this->renderChildrens($v->childrens);
			// 			}
			// 			echo '</li>';
		}
	}

	public function renderChildrens($childrens) {
		echo '<ul>';
		foreach ($childrens as $v) {
			$selected = '';
			if ($this->menuid == $v->id) {
				$selected = ' class="current-menu-item"';
			}
			echo '<li' . $selected . '><a href="#">' . $v->name . '<span class="menu">' . $v->note . '</span></a>';
			if (count($v->childrens) > 0) {
				$this->renderChildrens($v->childrens);
			}
			echo '</li>';
		}
		echo '</ul>';
	}

}
?>